﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Nustache.Mvc;
using MvcEnumFlags;
using WebMatrix.WebData;

namespace LadiesChoice
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            RegisterViewEngines(ViewEngines.Engines);
            ModelBinders.Binders.Add(typeof(Utilities.Enums.LookingFor), new EnumFlagsModelBinder());
            ModelBinders.Binders.Add(typeof(Utilities.Enums.EducationLevel), new EnumFlagsModelBinder());
            ModelBinders.Binders.Add(typeof(Utilities.Enums.HaveChildren), new EnumFlagsModelBinder());
            ModelBinders.Binders.Add(typeof(Utilities.Enums.WantChildren), new EnumFlagsModelBinder());
            ModelBinders.Binders.Add(typeof(Utilities.Enums.Drinks), new EnumFlagsModelBinder());
            ModelBinders.Binders.Add(typeof(Utilities.Enums.Smokes), new EnumFlagsModelBinder());
            ModelBinders.Binders.Add(typeof(Utilities.Enums.BodyTypeMen), new EnumFlagsModelBinder());
            ModelBinders.Binders.Add(typeof(Utilities.Enums.BodyTypeWomen), new EnumFlagsModelBinder());
            ModelBinders.Binders.Add(typeof(Utilities.Enums.Religion), new EnumFlagsModelBinder());
            System.Data.Entity.Database.SetInitializer<LadiesChoice.Utilities.DBEntities>(null);

        }
        public static void RegisterViewEngines(ViewEngineCollection engines)
        {
            engines.RemoveAt(0);
            engines.Add(new NustacheViewEngine
            {
                // Comment out this line to require Model in front of all your expressions.
                // This makes it easier to share templates between the client and server.
                // But it also means that ViewData/ViewBag is inaccessible.
                RootContext = NustacheViewEngineRootContext.Model
            });
        }

        void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            if (ex is HttpRequestValidationException)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.Write(@"
<html><head><title>HTML Not Allowed</title>
<script language='JavaScript'><!--
function back() { history.go(-1); } //--></script></head>
<body style='font-family: Arial, Sans-serif;'>
<h1>Oops!</h1>
<p>I'm sorry, but HTML entry is not allowed on that page.</p>
<p>Please make sure that your entries do not contain 
any angle brackets like &lt; or &gt;.</p>
<p><a href='javascript:back()'>Go back</a></p>
</body></html>
");
                Response.End();
            }
        }
    }
}