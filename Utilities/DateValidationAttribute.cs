﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LadiesChoice.Utilities
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class DateValidationAttribute : ValidationAttribute
    {
        public DateValidationAttribute(string errorMessage) :
            base(errorMessage) { }

        public override bool IsValid(object value)
        {
            bool result = false;
            if (value == null)
                throw new ArgumentNullException("value");

            Date toValidate = value as Date;

            if (toValidate == null)
                throw new ArgumentException("value is an invalid or is an unexpected type");

            //DateTime returns null when date cannot be constructed
            if (toValidate.DateTime != null)
            {
                result = (toValidate.DateTime != DateTime.MinValue) && (toValidate.DateTime != DateTime.MaxValue);
            }

            return result;
        }
    }
}