﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LadiesChoice.Utilities
{
    public class DateSelector_DropdownListBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");
            if (bindingContext == null)
                throw new ArgumentNullException("bindingContext");

            if (IsDropdownListBound(bindingContext))
            {
                int year = GetData(bindingContext, "Year");
                int month = GetData(bindingContext, "Month");
                int day = GetData(bindingContext, "Day");

                DateTime result;
                if (!DateTime.TryParse(string.Format("{0}/{1}/{2}", year, month, day), out result))
                {
                    //TODO: SOMETHING MORE USEFUL???
                    bindingContext.ModelState.AddModelError("", string.Format("Not a valid date."));
                }

                return result;
            }
            else
            {
                return base.BindModel(controllerContext, bindingContext);
            }

        }

        private int GetData(ModelBindingContext bindingContext, string propertyName)
        {
            // parse the int using the correct value provider
            return 999; //set by Paul to make this class compile!
        }

        private bool IsDropdownListBound(ModelBindingContext bindingContext)
        {
            //check model meta data UI hint for above editor template
            return false; //set by Paul to make this class compile!
        }
    }
}