﻿using LadiesChoice.Models;
using LadiesChoice.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.Utilities
{
    public static class MyMatchesSupport
    {
        public static IEnumerable<ProfileCardViewModel> getMyMatchProfiles(DBEntities db, MyPreferencesModel p, int viewerUserID, int limitRecords = 0)
        {
            int maleGenderCode = Enums.Gender.Male.GetHashCode();
            var profiles = from pf in db.ProfileModels
                           join ph in db.PhotoModels on pf.iUserID equals ph.UserID
                           join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                           join ar in db.AreaOfTownModels on pf.areaID equals ar.AreaOfTownModelID into joined
                           from j in joined.DefaultIfEmpty()
                           where ph.IsMainPhoto == true && pf.Gender == maleGenderCode && (pf.Drinks & p.Drinks) == pf.Drinks
                           && (pf.Smokes & p.Smokes) == pf.Smokes && (pf.Religion & p.Religion) == pf.Religion
                            && (pf.LookingFor & p.LookingFor) > 0 && (pf.BodyType & p.BodyType) == pf.BodyType
                            && p.TallerThan <= pf.Height && p.ShorterThan >= pf.Height
                            && (pf.Education & p.Education) == pf.Education && pf.Income >= p.MinimumIncome
                            && ur.IsDisabled == false && pf.BizzaroVisibility == false && ur.IsSuspectedSpammer == false
                           orderby ur.LastLoginDate descending


                           select new ProfileCardViewModel()
                           {
                               ProfileModelID = pf.ProfileModelID,
                               PhotoModelID = ph.PhotoModelID,
                               Area = j.Name,
                               Height = pf.Height,
                               Name = ur.UserName,
                               profile = true,
                               Age = 0,
                               BirthDate = pf.BirthDate,
                               LastLoginUTC = ur.LastLoginDate,
                               MatchUserID = ur.UserId
                           };
            if (limitRecords > 0)
            {
                profiles = profiles.Take(limitRecords);
            }
            List<ProfileCardViewModel> matchResults = new List<ProfileCardViewModel>();
            List<int> blockerblockeeList = getBlockedAndBlockeeList(viewerUserID, db);
            foreach (ProfileCardViewModel pm in profiles.ToList())
            {

                pm.Age = AgeHelper.getAgeToday(pm.BirthDate);
                if (pm.Age >= p.OlderThan && pm.Age <= p.YoungerThan)
                {
                    if (!blockerblockeeList.Contains(pm.MatchUserID))
                    {
                        matchResults.Add(pm);
                    }
                }
            }
            return matchResults;

        }

        public static IEnumerable<ProfileCardViewModel> getMyMutualMatchProfiles(DBEntities db, MyPreferencesModel p, int userID, bool isFemale = false, int limitRecords = 0, DateTime? newSinceDate = null)
        {
            ProfileModel cup = db.ProfileModels.FirstOrDefault(y => y.iUserID == userID);
            int viewersAge = AgeHelper.getAgeToday(cup.BirthDate);
            IQueryable<ProfileCardViewModel> profiles;

            DateTime filterFromDate;
            if (newSinceDate == null)
            {
                filterFromDate = DateTime.MinValue;
            }
            else
            {
                filterFromDate = DateTime.Parse(newSinceDate.ToString());
            }

            if (isFemale)
            {
                int genderCode = Enums.Gender.Male.GetHashCode();

                profiles = from pf in db.ProfileModels
                           join pref in db.MyPreferencesModels on pf.iUserID equals pref.UserID
                           join ph in db.PhotoModels on pf.iUserID equals ph.UserID
                           join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                           join ar in db.AreaOfTownModels on pf.areaID equals ar.AreaOfTownModelID into joined
                           from j in joined.DefaultIfEmpty()
       
                           where ph.IsMainPhoto == true && pf.Gender == genderCode && (pf.Drinks & p.Drinks) == pf.Drinks
                           && (pf.Smokes & p.Smokes) == pf.Smokes && (pf.Religion & p.Religion) == pf.Religion
                            && (pf.LookingFor & p.LookingFor) > 0 && (pf.BodyType & p.BodyType) == pf.BodyType
                            && p.TallerThan <= pf.Height && p.ShorterThan >= pf.Height
                            && (pf.Education & p.Education) == pf.Education && ur.IsDisabled == false
                            && (pref.Drinks & cup.Drinks) == cup.Drinks && (pref.Smokes & cup.Smokes) == cup.Smokes
                            && (pref.Religion & cup.Religion) == cup.Religion && (pref.BodyType & cup.BodyType) == cup.BodyType
                            && (pref.Education & cup.Education) == cup.Education && pref.TallerThan <= cup.Height
                            && pref.ShorterThan >= cup.Height && pf.BizzaroVisibility == false
                            && pf.Income >= p.MinimumIncome && pref.MinimumIncome <= cup.Income
                            && ur.CreationDate >= filterFromDate && ur.IsSuspectedSpammer == false
                           orderby ur.LastLoginDate descending


                           select new ProfileCardViewModel()
                           {
                               ProfileModelID = pf.ProfileModelID,
                               PhotoModelID = ph.PhotoModelID,
                               Area = j.Name,
                               Height = pf.Height,
                               Name = ur.UserName,
                               profile = true,
                               Age = 0,
                               BirthDate = pf.BirthDate,
                               LastLoginUTC = ur.LastLoginDate,
                               DesiredMinAge = pref.OlderThan,
                               DesireMaxAge = pref.YoungerThan,
                               MatchUserID = ur.UserId
                           };
            }
            else
            {
                int genderCode = Enums.Gender.Female.GetHashCode();
                
                profiles = from pf in db.ProfileModels
                           join pref in db.MyPreferencesModels on pf.iUserID equals pref.UserID
                           join ph in db.PhotoModels on pf.iUserID equals ph.UserID
                           join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                           join ar in db.AreaOfTownModels on pf.areaID equals ar.AreaOfTownModelID into joined
                           from j in joined.DefaultIfEmpty()
                           
                           where ph.IsMainPhoto == true && pf.Gender == genderCode && (pf.Drinks & p.Drinks) == pf.Drinks
                           && (pf.Smokes & p.Smokes) == pf.Smokes && (pf.Religion & p.Religion) == pf.Religion
                            && (pf.LookingFor & p.LookingFor) > 0 && (pf.BodyType & p.BodyType) == pf.BodyType
                            && p.TallerThan <= pf.Height && p.ShorterThan >= pf.Height
                            && (pf.Education & p.Education) == pf.Education && ur.IsDisabled == false
                            && (pref.Drinks & cup.Drinks) == cup.Drinks && (pref.Smokes & cup.Smokes) == cup.Smokes
                            && (pref.Religion & cup.Religion) == cup.Religion && (pref.BodyType & cup.BodyType) == cup.BodyType
                            && (pref.Education & cup.Education) == cup.Education && pref.TallerThan <= cup.Height
                            && pref.ShorterThan >= cup.Height && pf.VisibleToMatches == true
                            && pf.Income >= p.MinimumIncome && pref.MinimumIncome <= cup.Income
                            && ur.CreationDate >= filterFromDate && ur.IsSuspectedSpammer == false
                           orderby ur.LastLoginDate descending


                           select new ProfileCardViewModel()
                           {
                               ProfileModelID = pf.ProfileModelID,
                               PhotoModelID = ph.PhotoModelID,
                               Area = j.Name,
                               Height = pf.Height,
                               Name = ur.UserName,
                               profile = true,
                               Age = 0,
                               BirthDate = pf.BirthDate,
                               LastLoginUTC = ur.LastLoginDate,
                               DesiredMinAge = pref.OlderThan,
                               DesireMaxAge = pref.YoungerThan,
                               MatchUserID = ur.UserId
                           };

            }
            
            List<ProfileCardViewModel> matches = refineMatchData(userID, db, profiles, viewersAge, p);
            if (limitRecords > 0)
            {
                matches = matches.Take(limitRecords).ToList<ProfileCardViewModel>();
            }
            return matches;
        }

        private static List<int> getBlockedAndBlockeeList(int userID, DBEntities db)
        {
            List<int> blockedandblockeeList = new List<int>();
            IEnumerable<BlockedUserModel> bms = db.BlockedUserModels.Where(x => x.BlockerUserID == userID || x.BlockeeUserID == userID);
            if (bms == null) { return null; }
            foreach (BlockedUserModel b in bms)
            {
                if (b.BlockeeUserID == userID) { blockedandblockeeList.Add(b.BlockerUserID); } else { blockedandblockeeList.Add(b.BlockeeUserID); }
            }
            return blockedandblockeeList;
        }

        private static List<ProfileCardViewModel> refineMatchData(int userID, DBEntities db, IQueryable<ProfileCardViewModel> profiles, int viewersAge, MyPreferencesModel p)
        {
            List<ProfileCardViewModel> matchResults = new List<ProfileCardViewModel>();
            List<int> blockerblockeeList = getBlockedAndBlockeeList(userID, db);
            foreach (ProfileCardViewModel pm in profiles.ToList())
            {
                pm.Age = AgeHelper.getAgeToday(pm.BirthDate);
                if (pm.Age >= p.OlderThan && pm.Age <= p.YoungerThan && pm.DesiredMinAge <= viewersAge && pm.DesireMaxAge >= viewersAge)
                {
                    if (!blockerblockeeList.Contains(pm.MatchUserID))
                    {
                        matchResults.Add(pm);
                    }
                }
            }
            return matchResults;
        }
    }
}