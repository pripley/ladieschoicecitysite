﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using System.Configuration;


namespace LadiesChoice.Utilities
{
    public static class MessageSending
    {

        public static IRestResponse SendMessageFromLadiesChoice(string to, string subject, string text, string html = null) {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableEmailsToBeSent"))) { return null; }
            //text = text + to;
            //to = "pripley@gmail.com";
            RestClient client = new RestClient();
            client.BaseUrl = "https://api.mailgun.net/v2";
            client.Authenticator =
                    new HttpBasicAuthenticator("api",
                                               Properties.Settings.Default.MailGunAPIKey);
            RestRequest request = new RestRequest();

            request.AddParameter("domain",
                                 ConfigurationManager.AppSettings.Get("MailGunDomain"), ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", ConfigurationManager.AppSettings.Get("MailGunFrom"));
            request.AddParameter("to", to);
            request.AddParameter("subject", subject);
            request.AddParameter("text", text);
            if (html != null)
            {
                request.AddParameter("html", html);
            }
            request.Method = Method.POST;
            return client.Execute(request);
        }

        public static IRestResponse SendMessageToAdmin(string subject, string text) {
            return SendMessageFromLadiesChoice("pripley@gmail.com", subject, text);
        }
    }
}