﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.Utilities
{
    public class TimeZoneSupport
    {
        public static string SiteTimeZone = ConfigurationManager.AppSettings.Get("SiteTimeZone");
        private static TimeZoneInfo LocalTimeZone = TimeZoneInfo.FindSystemTimeZoneById(SiteTimeZone);

        public static DateTime GetLocalTime(DateTime d) {
            return TimeZoneInfo.ConvertTimeFromUtc(d, LocalTimeZone);
        }
        public static DateTime GetLocalTime(DateTime d, string timeZone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(d, TimeZoneInfo.FindSystemTimeZoneById(timeZone));
        }
        public static string GetFriendlyTime(DateTime input)
        {
            TimeSpan oSpan = DateTime.Now.Subtract(input);
            double TotalMinutes = oSpan.TotalMinutes;
            //string Suffix = " ago";

            //if (TotalMinutes < 0.0)
            //{
            //    TotalMinutes = Math.Abs(TotalMinutes);
            //    Suffix = " from now";
            //}

            var aValue = new SortedList<double, Func<string>>();
            aValue.Add(0.75, () => "Online now!");
            aValue.Add(1.5, () => "Online now!");
            aValue.Add(45, () => string.Format("{0} min ago", Math.Round(TotalMinutes)));
            aValue.Add(90, () => "1 hour ago");
            aValue.Add(1440, () => string.Format("{0} hours ago", Math.Round(Math.Abs(oSpan.TotalHours)))); // 60 * 24
            aValue.Add(2880, () => "yesterday"); // 60 * 48
            aValue.Add(43200, () => string.Format("{0} days ago", Math.Floor(Math.Abs(oSpan.TotalDays)))); // 60 * 24 * 30
            aValue.Add(86400, () => "1 month ago"); // 60 * 24 * 60
            aValue.Add(525600, () => string.Format("{0} months ago", Math.Floor(Math.Abs(oSpan.TotalDays / 30)))); // 60 * 24 * 365 
            aValue.Add(1051200, () => "a year ago"); // 60 * 24 * 365 * 2
            aValue.Add(double.MaxValue, () => string.Format("{0} years ago", Math.Floor(Math.Abs(oSpan.TotalDays / 365))));

            return aValue.First(n => TotalMinutes < n.Key).Value.Invoke();
        }
        public static string GetFriendlyTimeAlt(DateTime input)
        {
            TimeSpan oSpan = DateTime.Now.Subtract(input);
            double TotalMinutes = oSpan.TotalMinutes;

            var aValue = new SortedList<double, Func<string>>();
            aValue.Add(0.75, () => "just now!");
            aValue.Add(1.5, () => "just now!");
            aValue.Add(45, () => string.Format("{0} min ago", Math.Round(TotalMinutes)));
            aValue.Add(90, () => "1 hour ago");
            aValue.Add(1440, () => string.Format("{0} hours ago", Math.Round(Math.Abs(oSpan.TotalHours)))); // 60 * 24
            aValue.Add(2880, () => "yesterday"); // 60 * 48
            aValue.Add(43200, () => string.Format("{0} days ago", Math.Floor(Math.Abs(oSpan.TotalDays)))); // 60 * 24 * 30
            aValue.Add(86400, () => "1 month ago"); // 60 * 24 * 60
            aValue.Add(525600, () => string.Format("{0} months ago", Math.Floor(Math.Abs(oSpan.TotalDays / 30)))); // 60 * 24 * 365 
            aValue.Add(1051200, () => "a year ago"); // 60 * 24 * 365 * 2
            aValue.Add(double.MaxValue, () => string.Format("{0} years ago", Math.Floor(Math.Abs(oSpan.TotalDays / 365))));

            return aValue.First(n => TotalMinutes < n.Key).Value.Invoke();
        }
    }
}