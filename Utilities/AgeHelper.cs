﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.Utilities
{
    public static class AgeHelper
    {
        public static int getAgeToday(DateTime dt)
        {
            var ageYear = DateTime.Today.Year - dt.Year;
            var ageMonth = DateTime.Today.Month - dt.Month;
            var ageDay = DateTime.Today.Day - dt.Day;
            if ((ageMonth == 0 && ageDay > 0) || ageMonth > 0)
            {
                return ageYear;
            }
            else
            {
                return ageYear - 1;
            }
          
        }
    }
}