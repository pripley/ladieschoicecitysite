﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace LadiesChoice.Utilities
{
    public static class CountryIPList
    {
        public static long[] CountryIPAddresses = null;
        
        public static void InitializeCountryIPList() {
            if (CountryIPAddresses != null) { return; }
            FileInfo fi = new FileInfo(System.Web.HttpContext.Current.Server.MapPath("/Content/CanadianIPRanages.csv"));
            StreamReader reader = fi.OpenText();
            string line;
            int i = 0;
            var ipList = new List<long>();
            while ((line = reader.ReadLine()) != null) {
                string[] items;
                items = line.Split(',');
                ipList.Add(long.Parse(items[0]));
                ipList.Add(long.Parse(items[1]));
                i += 2;
            }

            CountryIPAddresses = ipList.ToArray();
            reader.Close();
        }

        public static bool isIPWithinCountry(string ip) {
            //long ipInt = (long)(uint)IPAddress.NetworkToHostOrder((int)IPAddress.Parse(ip).Address);
            //long ipInt = BitConverter.ToInt32(IPAddress.Parse(ip).GetAddressBytes().Reverse().ToArray(), 0);
            long ipInt = ConvertIPToLong(ip);
            if (CountryIPAddresses == null) { InitializeCountryIPList(); }
            for (int i = 0; i < CountryIPAddresses.Length; i += 2)
            {
                if (CountryIPAddresses[i] <= ipInt && ipInt <= CountryIPAddresses[i + 1])
                {
                    return true;
                }
            }
            return false;
        }

        public static long ConvertIPToLong(string ipAddress)
        {
            System.Net.IPAddress ip;

            if (System.Net.IPAddress.TryParse(ipAddress, out ip))
            {
                byte[] bytes = ip.GetAddressBytes();
                

                return (long)
                    (
                    16777216 * (long)bytes[0] +
                    65536 * (long)bytes[1] +
                    256 * (long)bytes[2] +
                    (long)bytes[3]
                    )
                    ;
            }
            else
                return 0;
        }
    }
}