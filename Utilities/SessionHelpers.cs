﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.Utilities
{
    public static class SessionHelpers
    {
        public static void SaveGenderInSession(HttpContextBase x, int gender)
        {
            x.Session["gender"] = gender.ToString();
        }
        public static bool isFemale(HttpContextBase x)
        {
            try
            {
                if (x.Session["gender"].Equals(Enums.Gender.Female.GetHashCode().ToString()))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch {
                throw new Exception("Having some issues retrieving gender from session...");
            }
        }
    }
}