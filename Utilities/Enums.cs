﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LadiesChoice.Utilities
{
    public class Enums
    {
        public const int DEFAULT_OLDER_THAN = 18;
        public const int DEFAULT_YOUNGER_THAN = 99;

        
        public enum Gender
        {
            Male,
            Female
        }

        [Flags]
        public enum Smokes
        {
            No = 1,
            Yes = 2,
            Sometimes = 4
        }

        [Flags]
        public enum Drinks
        {
            No = 1,
            [Display(Name = "Yes, Often")]
            Yes = 2,
            [Display(Name = "Yes, socially")]
            Socially = 4
        }

        [Flags]
        public enum BodyTypeMen
        {
            Average = 1,
            Slim = 2,
            Athletic = 4,
            [Display(Name = "More to Love")]
            MoreToLove = 8
        }

        [Flags]
        public enum BodyTypeWomen
        {
            Average = 1,
            Svelte = 2,
            Athletic = 4,
            Curvy = 8
        }

        [Flags]
        public enum LookingFor
        {
            [Display(Name = "Casual Dating")]
            Casual = 1,
            Relationship = 2,
            Friends = 4
        }

        [Flags]
        public enum EducationLevel
        {
            [Display(Name = "High School")]
            HighSchool = 1,
            [Display(Name = "Some College")]
            College = 2,
            Bachelors = 4,
            Masters = 8,
            PhD = 16
        }

        [Flags]
        public enum Religion
        {
            [Display(Name = "Not Religious")]
            NotReligious = 1,
            Agnostic = 2,
            Protestant = 4,
            Catholic = 8,
            Muslim = 16,
            Hindu = 32,
            Buddist = 64,
            Sikh = 128,
            Jewish = 256,
            Other = 512
        }
        [Flags]
        public enum HaveChildren {
            No = 1,
            Yes = 2,
            [Display(Name = "Yes, and they sometimes live at home")]
            YesSometimesAtHome = 4,
            [Display(Name = "Yes, and they don't live at home")]
            YesDoNotLiveAtHome = 8
        }
        [Flags]
        public enum WantChildren { 
            Maybe = 1,
            Yes = 2,
            No = 4,
        }
        [Flags]
        public enum Income { 
            [Display(Name = "Under $20,000")]
            Under20K = 0,
            [Display(Name = "$20,000 - $29,000")]
            R20to30K = 1,
            [Display(Name = "$30,000 - $49,000")]
            R30to50K = 2,
            [Display(Name = "$50,000 - $79,000")]
            R50to80K = 4,
            [Display(Name = "$80,000 - $100,000")]
            R80to100K = 8,
            [Display(Name = "More than $100,000")]
            Over100K = 16
        }

        public enum MinimumIncome
        {
            [Display(Name = "Doesn't matter")]
            zero = 0,
            [Display(Name = "$20,000")]
            R20K = 1,
            [Display(Name = "$30,000")]
            R30K = 2,
            [Display(Name = "$50,000")]
            R50K = 4,
            [Display(Name = "$80,000")]
            R80K = 8,        
            [Display(Name = "$100,000")]
            R100K = 16
        }

        public enum Height {
            [Display(Name = "4'0\"")]
            Fo0,
            [Display(Name = "4'1\"")]
            Fo1,
            [Display(Name = "4'2\"")]
            Fo2,
            [Display(Name = "4'3\"")]
            Fo3,
            [Display(Name = "4'4\"")]
            Fo4,
            [Display(Name = "4'5\"")]
            Fo5,
            [Display(Name = "4'6\"")]
            Fo6,
            [Display(Name = "4'7\"")]
            Fo7,
            [Display(Name = "4'8\"")]
            Fo8,
            [Display(Name = "4'9\"")]
            Fo9,
            [Display(Name = "4'10\"")]
            Fo10,
            [Display(Name = "4'11\"")]
            Fo11,
            [Display(Name = "5'0\"")]
            Fv0,
            [Display(Name = "5'1\"")]
            Fv1,
            [Display(Name = "5'2\"")]
            Fv2,
            [Display(Name = "5'3\"")]
            Fv3,
            [Display(Name = "5'4\"")]
            Fv4,
            [Display(Name = "5'5\"")]
            Fv5,
            [Display(Name = "5'6\"")]
            Fv6,
            [Display(Name = "5'7\"")]
            Fv7,
            [Display(Name = "5'8\"")]
            Fv8,
            [Display(Name = "5'9\"")]
            Fv9,
            [Display(Name = "5'10\"")]
            Fv10,
            [Display(Name = "5'11\"")]
            Fv11,
            [Display(Name = "6'0\"")]
            Sx0,
            [Display(Name = "6'1\"")]
            Sx1,
            [Display(Name = "6'2\"")]
            Sx2,
            [Display(Name = "6'3\"")]
            Sx3,
            [Display(Name = "6'4\"")]
            Sx4,
            [Display(Name = "6'5\"")]
            Sx5,
            [Display(Name = "6'6\"")]
            Sx6,
            [Display(Name = "6'7\"")]
            Sx7,
            [Display(Name = "6'8\"")]
            Sx8,
            [Display(Name = "6'9\"")]
            Sx9,
            [Display(Name = "6'10\"")]
            Sx10,
            [Display(Name = "6'11\"")]
            Sx11,
            [Display(Name = "7'0\"")]
            Sv0,
            [Display(Name = "7'1\"")]
            Sv1,
            [Display(Name = "7'2\"")]
            Sv2,
            [Display(Name = "7'3\"")]
            Sv3,
            [Display(Name = "7'4\"")]
            Sv4,
            [Display(Name = "7'5\"")]
            Sv5
        }
    }
}
