﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.Web.Routing;
using System.Web.Mvc.Html;
using System.Web;
using System.Text;
using System.ComponentModel;
using System.Web.UI.WebControls;

namespace LadiesChoice.Utilities
{
    public static class EnumExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectList<T>(this T selected, bool areValuesInts = true)
            where T : struct, IConvertible
        {
            var type = typeof(T);

            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be an enum type.");
            }

            foreach (var value in Enum.GetValues(type))
            {
                var attribute = type.GetField(value.ToString()).GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault() as DisplayAttribute;
                string name = attribute != null ? attribute.GetName() : Enum.GetName(type, value);

                if (areValuesInts)
                {
                    yield return new SelectListItem { Selected = selected.Equals(value), Text = name, Value = ((int)value).ToString() };
                }
                else
                {
                    yield return new SelectListItem { Selected = selected.Equals(value), Text = name, Value = value.ToString() };
                }
            }
        }

        public static string EnumValueToDisplayString<T>(int value) where T : struct, IConvertible
        {
            var type = typeof(T);

            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be an enum type.");
            }

            
            var v1 = type.GetEnumName(value);
            var attribute1 = type.GetField(v1);
            var attribute = attribute1.GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault() as DisplayAttribute;
            string name = attribute != null ? attribute.GetName() : Enum.GetName(type, value);
            return name;
        }

        public static string EnumMultiValuesToDisplayString<T>(int value, bool hideIfAll = false) where T : struct, IConvertible
        {
            var type = typeof(T);

            if (!type.IsEnum)
            {
                throw new ArgumentException("T must be an enum type.");
            }
            string s = String.Empty;
            int hc;
            string name;
            DisplayAttribute attribute;
            bool isAllValues = true;
            foreach (Enum en in Enum.GetValues(type))
            {
                hc = en.GetHashCode();
                if ((hc & value) == hc)
                {  //see if a bitwise "and" is true i.e. is the flag set
                    attribute = type.GetField(en.ToString()).GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault() as DisplayAttribute;
                    name = attribute != null ? attribute.GetName() : Enum.GetName(type, en);
                    if (s.Equals(String.Empty))
                    {
                        s = name;
                    }
                    else
                    {
                        s += ", " + name;
                    }
                }
                else
                {
                    isAllValues = false;
                }
            }
            if (hideIfAll && isAllValues) {
                return "";
            }
            return s;
        }
        //public static IEnumerable<CheckBox> ToCheckBoxFlagList<T>(this T selected)
        //    where T : struct, IConvertible
        //{
        //    var type = typeof(T);

        //    if (!type.IsEnum)
        //    {
        //        throw new ArgumentException("T must be an enum type.");
        //    }

        //    foreach (var value in Enum.GetValues(type))
        //    {
        //        var attribute = type.GetField(value.ToString()).GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault() as DisplayAttribute;
        //        string name = attribute != null ? attribute.GetName() : Enum.GetName(type, value);

        //        //yield return new SelectListItem { Selected = selected.Equals(value), Text = name, Value = value.ToString() };
        //        if (model.HasFlag(item))

        //        yield return new CheckBox { Checked = ., Text = name };
        //    }
        //}

        //public static IHtmlString CheckBoxFlagList(string name, IEnumerable<CheckBox> checkBoxList) {
        //    foreach (CheckBox checkBox in checkBoxList)
        //    {
        //        checkBox.. = name;

        //    }
        //    return new HtmlString("<p>hello world</p>"); 
        //}
        //public static MvcHtmlString CheckBoxListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty[]>> expression, MultiSelectList multiSelectList, object htmlAttributes = null)
        //{
        //    //Derive property name for checkbox name
        //    MemberExpression body = expression.Body as MemberExpression;
        //    string propertyName = body.Member.Name;

        //    //Get currently select values from the ViewData model
        //    TProperty[] list = expression.Compile().Invoke(htmlHelper.ViewData.Model);

        //    //Convert selected value list to a List<string> for easy manipulation
        //    List<string> selectedValues = new List<string>();

        //    if (list != null)
        //    {
        //        selectedValues = new List<TProperty>(list).ConvertAll<string>(delegate(TProperty i) { return i.ToString(); });
        //    }

        //    //Create div
        //    TagBuilder divTag = new TagBuilder("div");
        //    divTag.MergeAttributes(new RouteValueDictionary(htmlAttributes), true);

        //    //Add checkboxes
        //    foreach (SelectListItem item in multiSelectList)
        //    {
        //        divTag.InnerHtml += String.Format("<div><input type=\"checkbox\" name=\"{0}\" id=\"{0}_{1}\" " +
        //                                            "value=\"{1}\" {2} /><label for=\"{0}_{1}\">{3}</label></div>",
        //                                            propertyName,
        //                                            item.Value,
        //                                            selectedValues.Contains(item.Value) ? "checked=\"checked\"" : "",
        //                                            item.Text);
        //    }

        //    return MvcHtmlString.Create(divTag.ToString());
        //}


       

        public static IHtmlString CheckBoxesForEnumFlagsFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, string checkBoxListClass)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumModelType = metadata.ModelType;

            // Check to make sure this is an enum.
            if (!enumModelType.IsEnum)
            {
                throw new ArgumentException("This helper can only be used with enums. Type used was: " + enumModelType.FullName.ToString() + ".");
            }

            // Create string for Element.
            var sb = new StringBuilder();
            sb.AppendLine("<ul class=\"" + checkBoxListClass + "\">");
            foreach (Enum item in Enum.GetValues(enumModelType))
            {
                if (Convert.ToInt32(item) != 0)
                {
                    var ti = htmlHelper.ViewData.TemplateInfo;
                    var id = ti.GetFullHtmlFieldId(item.ToString());
                    var name = ti.GetFullHtmlFieldName(metadata.PropertyName);
                    var label = new TagBuilder("label");
                    label.Attributes["for"] = id;
                    var field = item.GetType().GetField(item.ToString());

                    // Add checkbox.
                    var checkbox = new TagBuilder("input");
                    checkbox.Attributes["id"] = id;
                    checkbox.Attributes["name"] = name;
                    checkbox.Attributes["type"] = "checkbox";
                    checkbox.Attributes["value"] = item.ToString();
                    var model = metadata.Model as Enum;
                    if (model.HasFlag(item))
                    {
                        checkbox.Attributes["checked"] = "checked";
                    }
                    sb.AppendLine("<li>");
                    sb.AppendLine(checkbox.ToString());

                    // Check to see if DisplayName attribute has been set for item.
                    var displayName = field.GetCustomAttributes(typeof(DisplayNameAttribute), true)
                        .FirstOrDefault() as DisplayNameAttribute;
                    if (displayName != null)
                    {
                        // Display name specified.  Use it.
                        label.SetInnerText(displayName.DisplayName);
                    }
                    else
                    {
                        // Check to see if Display attribute has been set for item.
                        var display = field.GetCustomAttributes(typeof(DisplayAttribute), true)
                            .FirstOrDefault() as DisplayAttribute;
                        if (display != null)
                        {
                            label.SetInnerText(display.Name);
                        }
                        else
                        {
                            label.SetInnerText(item.ToString());
                        }
                    }
                    sb.AppendLine(label.ToString());

                    // close list item tag
                    sb.AppendLine("</li>");
                }
            }
            sb.AppendLine("</ul>");
            return new HtmlString(sb.ToString());
        }


        //public static MvcHtmlString EnumDropDownList<TEnum>(this HtmlHelper htmlHelper, string name, TEnum selectedValue)
        //{
        //    IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum))
        //        .Cast<TEnum>();

        //    IEnumerable<SelectListItem> items =
        //        from value in values
        //        select new SelectListItem
        //        {
        //            Text = value.ToString(),
        //            Value = value.ToString(),
        //            Selected = (value.Equals(selectedValue))
        //        };

        //    return htmlHelper.DropDownList(
        //        name,
        //        items
        //        );
        //}



        //public static SelectList GetGenderSelectList()
        //{

        //    var enumValues = Enum.GetValues(typeof(LadiesChoice.Models.ProfileModel.Gender)).Cast<LadiesChoice.Models.ProfileModel.Gender>().Select(e => new { Value = e.ToString(), Text = e.ToString() }).ToList();

        //    return new SelectList(enumValues, "Value", "Text", "");
        //}
    }
}