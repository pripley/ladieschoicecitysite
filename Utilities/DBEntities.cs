﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using LadiesChoice.Models;
using System.Linq.Expressions;


namespace LadiesChoice.Utilities
{
    public class DBEntities : DbContext
    {
        public DbSet<ProfileModel> ProfileModels { get; set; }
        public DbSet<AreaOfTownModel> AreaOfTownModels { get; set; }
        public DbSet<MyPreferencesModel> MyPreferencesModels { get; set; }
        public DbSet<PhotoModel> PhotoModels { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<MessageModel> MessageModels { get; set; }
        public DbSet<BlockedUserModel> BlockedUserModels { get; set; }
        public DbSet<SiteHandshakeModel> SiteHandshakeModels { get; set; }
        public DbSet<GiftModel> GiftModels { get; set; }
        public DbSet<EventModel> EventModels { get; set; }

        //

        //public DbSet<ProfileModel> GetEditProfileValues() {
        //    this.
        //}

        public DBEntities() : base("DefaultConnection") {}

        public DBEntities(string conn) : base(conn) { }

    }
}