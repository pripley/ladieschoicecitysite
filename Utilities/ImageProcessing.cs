﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.Models;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Configuration;

namespace LadiesChoice.Utilities
{
    public static class ImageProcessing
    {
        public static void resizeImage(HttpPostedFileBase file, string newFileName) {
            if (file != null && file.ContentLength > 0)
            {
                // extract only the filename
                //var fileName = Path.GetFileName(file.FileName);
                
                var fileName = newFileName;
                // store the file inside ~/App_Data/uploads folder
                //var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("PhotoPath")), fileName);
                
                Image image = Image.FromStream(file.InputStream);
                
                bool imageInLandscapeOrientation = false;
                int resizedImageWidth;
                int resizedImageHeight;
                if (image.Width <= 640 & image.Height <= 640)
                {
                    resizedImageWidth = image.Width;
                    resizedImageHeight = image.Height;
                }
                if (image.Width > image.Height)
                {
                    resizedImageWidth = 640;
                    resizedImageHeight = image.Height * 640 / image.Width;
                    imageInLandscapeOrientation = true;
                }
                else
                {
                    resizedImageHeight = 640;
                    resizedImageWidth = image.Width * 640 / image.Height;
                }

                Bitmap target = new Bitmap(resizedImageWidth, resizedImageHeight);
                using (Graphics graphics = Graphics.FromImage(target))
                {
                    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.DrawImage(image, 0, 0, resizedImageWidth, resizedImageHeight);
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        target.Save(memoryStream, ImageFormat.Jpeg);
                    }
                }
                
                Image mainImage = target;

                //local storage of images
                //path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("PhotoPath")), "main_" + fileName);
                //mainImage.Save(path);

                MemoryStream memStream = new MemoryStream();
                mainImage.Save(memStream, ImageFormat.Jpeg);
                AWSFunctions awsf = new AWSFunctions();
                awsf.UploadFile(memStream, "main/" + fileName);

                int thumbnailWidth;
                int thumbnailHeight;

                if (imageInLandscapeOrientation)
                {
                    thumbnailWidth = image.Height;
                    thumbnailHeight = image.Height;
                }
                else
                {
                    thumbnailHeight = image.Width;
                    thumbnailWidth = image.Width;
                }


                Bitmap original = new Bitmap(image);


                Point loc = new Point(0, 0);
                Size cropSize = new Size(image.Width, image.Height);

                Rectangle cropArea = new Rectangle(loc, cropSize);



                Bitmap bmpCrop = CropImage(original, cropArea);


                //path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("PhotoPath")), "thumb_" + fileName);
                Image thumb = bmpCrop.GetThumbnailImage(150, 150, () => false, IntPtr.Zero);
                
                //local storage of images
                //thumb.Save(path);
                memStream = new MemoryStream();
                thumb.Save(memStream, ImageFormat.Jpeg);
                awsf.UploadFile(memStream, "thumb/" + fileName);

                image.Dispose();
            }
        }
        public static Bitmap CropImage(Bitmap source, Rectangle section)
        {
            // An empty bitmap which will hold the cropped image  
            Bitmap bmp = new Bitmap(section.Width, section.Height);
            Graphics g = Graphics.FromImage(bmp);
            // Draw the given area (section) of the source image  
            // at location 0,0 on the empty bitmap (bmp)  
            g.DrawImage(source, 0, 0, section, GraphicsUnit.Pixel);
            return bmp;
        } 
    }
}