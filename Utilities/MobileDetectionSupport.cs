﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.Utilities
{
    public class MobileDetectionSupport
    {
        private static string[] mobileDevices = new string[] {"iphone","ipad" };

        public static bool IsMobileDevice(string userAgent)
        {
            if (userAgent == null) { return false; }
            userAgent = userAgent.ToLower();
            return mobileDevices.Any(x => userAgent.Contains(x));
        }
    }
}