﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

namespace LadiesChoice.Utilities
{
    public class AWSFunctions //: IDisposable
    {

        private string awsAccessKeyId = Properties.Settings.Default.AWSAccessKey;
        private string awsSecretAccessKey = Properties.Settings.Default.AWSSecretKey;
        private string bucketName = ConfigurationManager.AppSettings.Get("AWSBucketName");
        //private Amazon.S3.Transfer.TransferUtility transferUtility;
        //private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AWSFunctions()
        {
            // Initialize log4net.
            //log4net.Config.XmlConfigurator.Configure();
            //this.transferUtility = new Amazon.S3.Transfer.TransferUtility(awsAccessKeyId, awsSecretAccessKey);
            //log.Info("S3 instance initiated");

        }

        public void UploadFile(System.IO.Stream stream, string toPath)
        {

            try
            {
                AmazonS3 client = AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSecretAccessKey);

                PutObjectRequest request = new PutObjectRequest();
                request.WithBucketName(bucketName)
                    .WithKey(toPath)
                    .WithInputStream(stream)
                    .AddHeader("x-amz-acl", "public-read");
           
                S3Response response = client.PutObject(request);
                response.Dispose();
                
        //        AsyncCallback callback = new AsyncCallback(uploadComplete);
        //        var uploadRequest = new Amazon.S3.Transfer.TransferUtilityUploadRequest();
        //        //uploadRequest.FilePath = filePath;
        //        uploadRequest.InputStream = stream;
        //        uploadRequest.BucketName = bucketName;
        //        uploadRequest.Key = toPath;
        //        uploadRequest.StorageClass = Amazon.S3.Model.S3StorageClass.Standard;
        //        uploadRequest.AddHeader("x-amz-acl", "public-read");
        //        Amazon.S3.Transfer.TransferUtility transferUtility = new Amazon.S3.Transfer.TransferUtility(awsAccessKeyId, awsSecretAccessKey);
        //        IAsyncResult ar = transferUtility.BeginUpload(uploadRequest, callback, null);
        //        ThreadPool.QueueUserWorkItem(c =>
        //        {
        //            try
        //            {
        //                transferUtility.EndUpload(ar);
        //            }
        //            catch (AmazonS3Exception ex)
        //            {
        //                Console.WriteLine(ex.ErrorCode);
        //            }
        //            catch (WebException ex)
        //            {
        //                if (ex.InnerException != null)
        //                {
        //                    Console.WriteLine(ex.InnerException.ToString());
        //                }
        //            }
        //        });
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                Console.WriteLine(amazonS3Exception.ErrorCode);
                //log.ErrorFormat("An Error, number {0}, occurred when creating a bucket with the message '{1}", amazonS3Exception.ErrorCode, amazonS3Exception.Message);
            }
        }


        public void UploadFile(string filePath, string toPath)
        {

            try
            {
                AmazonS3 client = AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSecretAccessKey);

                PutObjectRequest request = new PutObjectRequest();
                request.WithBucketName(bucketName)
                    .WithKey(toPath)
                    .WithFilePath(filePath)
                    .AddHeader("x-amz-acl", "public-read");

                S3Response response = client.PutObject(request);
                response.Dispose();

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                Console.WriteLine(amazonS3Exception.ErrorCode);
                //log.ErrorFormat("An Error, number {0}, occurred when creating a bucket with the message '{1}", amazonS3Exception.ErrorCode, amazonS3Exception.Message);
            }
        }
        //private void uploadComplete(IAsyncResult result)
        //{
        //    var x = result;

        //    //if (x.IsCompleted) { 
                
        //    //}
        //}

        public void DeleteFile(string filePath)
        {
            try
            {
                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest()
                .WithBucketName(bucketName)
                .WithKey(filePath);
                Amazon.S3.Transfer.TransferUtility transferUtility = new Amazon.S3.Transfer.TransferUtility(awsAccessKeyId, awsSecretAccessKey);
                transferUtility.S3Client.DeleteObject(deleteObjectRequest);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}