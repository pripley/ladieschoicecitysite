﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace LadiesChoice.Utilities
{
    public class Date
    {
        public Date() : this(System.DateTime.MinValue) { }
        public Date(DateTime date)
        {
            Year = date.Year;
            Month = date.Month;
            Day = date.Day;
        }

        [Required]
        public int Year { get; set; }

        [Required, Range(1, 12)]
        public int Month { get; set; }

        [Required, Range(1, 31)]
        public int Day { get; set; }

        public DateTime? DateTime
        {
            get
            {
                DateTime date;
                if (!System.DateTime.TryParseExact(string.Format("{0}/{1}/{2}", Year, Month, Day), "yyyy/M/d", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    return null;
                else
                    return date;
            }
        }
    }
}