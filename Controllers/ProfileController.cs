﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.Models;
using LadiesChoice.ViewModels;
using AutoMapper;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;
using LadiesChoice.Utilities;
using WebMatrix.WebData;
using LadiesChoice.Filters;

namespace LadiesChoice.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class ProfileController : Controller
    {
        private Utilities.DBEntities db = new Utilities.DBEntities();

        public ActionResult MyProfile(string flashMessage = null) {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Profile/MyProfile" }); }
            MyProfileViewModel vpv = new MyProfileViewModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            vpv.FlashMessage = flashMessage;
            if (vpv.FlashMessage == null && vpv.HasUserDisabledTheirAccount) {
                vpv.FlashMessage = "Your account is currently disabled.  You can re-enable it on the My Account page";
            }
            return View(vpv);
        }

        //View other people's profiles
        public ActionResult Index(Guid id, string flashMessage = null)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Profile/Index/" +id }); }

            ProfileModel profilemodel = db.ProfileModels.FirstOrDefault(x => x.ProfileModelID == id);
            if (profilemodel == null)
            {
                return HttpNotFound();
            }
            if (profilemodel.iUserID == WebSecurity.CurrentUserId) { //check if the user is trying to view their own profile
                return RedirectToAction("MyProfile");
            }

            if (db.BlockedUserModels.Count(y => y.BlockeeUserID == WebSecurity.CurrentUserId && y.BlockerUserID == profilemodel.iUserID) > 0)
            {
                return RedirectToAction("MyProfile", new { flashMessage = "Sorry, that user has disabled their account." });  //instead of saying "you were blocked" we say "disabled" to be kinder
            }

            ViewProfileViewModel vpv = new ViewProfileViewModel(profilemodel.iUserID, this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            vpv.FlashMessage = flashMessage;
            if (vpv.HasUserDisabledTheirAccount) {
                return RedirectToAction("MyProfile", new { flashMessage = "Sorry, " + vpv.UsernameOfProfile + " has disabled their profile."});
            }

            return View("ViewProfile", vpv);
        }

        
        public ActionResult EditProfile(string flashMessage=null)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Profile/EditProfile/" }); }

            EditProfileViewModel editProfileViewModel = new EditProfileViewModel();

            
            var g = WebSecurity.CurrentUserId;
            UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserId == g);
            ProfileModel p = db.ProfileModels.FirstOrDefault(x => x.iUserID == g);
            
            
            Mapper.CreateMap<ProfileModel, EditProfileViewModel>();
            editProfileViewModel = Mapper.Map<ProfileModel, EditProfileViewModel>(p);
            editProfileViewModel.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db, u.HasCompletedProfile);
            editProfileViewModel.BirthDateComposite = new Utilities.Date(p.BirthDate);
            editProfileViewModel.EmailAndUsernameUserProfile = u;


            editProfileViewModel.FlashMessage = flashMessage;

            editProfileViewModel.setBodyTypeList();

            if (editProfileViewModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.areaID = new SelectList(db.AreaOfTownModels, "AreaOfTownModelID", "Name", editProfileViewModel.areaID);
            return View(editProfileViewModel);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(EditProfileViewModel editProfileViewModel)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Profile/EditProfile/" }); }

            if (ModelState.IsValid)
            {
                Mapper.CreateMap<EditProfileViewModel, ProfileModel>();
                ProfileModel profile = Mapper.Map<EditProfileViewModel, ProfileModel>(editProfileViewModel);
                if (editProfileViewModel.BirthDateComposite.DateTime == null)
                {
                    ModelState.AddModelError("", "Please select a valid birth date.");
                }
                else
                {
                    profile.BirthDate = (DateTime)editProfileViewModel.BirthDateComposite.DateTime;
                }
                profile.iUserID = WebSecurity.CurrentUserId;

                if (db.UserProfiles.Count(x => x.Email.ToLower() == editProfileViewModel.EmailAndUsernameUserProfile.Email.ToLower() && x.UserId != WebSecurity.CurrentUserId) > 0)
                {
                    ModelState.AddModelError("", "This email is already associated with another account in the system.");
                }

                if (editProfileViewModel.EmailAndUsernameUserProfile.UserName.ToLower() != User.Identity.Name.ToLower())
                {
                    if (db.UserProfiles.Count(x => x.UserName == editProfileViewModel.EmailAndUsernameUserProfile.UserName) > 0)
                    {
                        ModelState.AddModelError("", "This username is already associated with another account in the system.");
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        FormsAuthentication.SetAuthCookie(editProfileViewModel.EmailAndUsernameUserProfile.UserName, true);
                    }
                }      

                if (ModelState.IsValid)
                {
                    UserProfile userInfo = db.UserProfiles.FirstOrDefault(x=> x.UserId == WebSecurity.CurrentUserId);
                    bool hasPreviouslyCompletedProfile = userInfo.HasCompletedProfile;
                    userInfo.Email = editProfileViewModel.EmailAndUsernameUserProfile.Email;
                    userInfo.UserName = editProfileViewModel.EmailAndUsernameUserProfile.UserName;
                    userInfo.HasCompletedProfile = true;
                    if (CheckForEmailAddressesInProfileText(profile)) {
                        userInfo.IsSuspectedSpammer = true;
                        MessageSending.SendMessageToAdmin("Possible email in profile", userInfo.UserName);
                    }
                    db.Entry(userInfo).State = EntityState.Modified;
                    db.Entry(profile).State = EntityState.Modified;
                    db.SaveChanges();
                    if (hasPreviouslyCompletedProfile) {
                        return RedirectToAction("EditProfile", "Profile", new { flashMessage = "Profile Saved." });
                    } else {
                        return RedirectToAction("MyPhotos", "Photo", new { flashMessage = "Profile Saved." });
                    }                    
                }     
            }
            ViewBag.areaID = new SelectList(db.AreaOfTownModels, "AreaOfTownModelID", "Name", editProfileViewModel.areaID);
            editProfileViewModel.setBodyTypeList();
            editProfileViewModel.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db, db.UserProfiles.FirstOrDefault(x=> x.UserId == WebSecurity.CurrentUserId).HasCompletedProfile);
            return View(editProfileViewModel);
        }

        private bool CheckForEmailAddressesInProfileText(ProfileModel p) { 
            if (p.Description != null && p.Description.Contains("@")) {
                return true;
            }
            if (p.Hobbies != null && p.Hobbies.Contains("@"))
            {
                return true;
            }
            if (p.ValueInAPartner != null && p.ValueInAPartner.Contains("@"))
            {
                return true;
            }
            if (p.WhatILikeAboutMyCity != null && p.WhatILikeAboutMyCity.Contains("@"))
            {
                return true;
            }
            if (p.ExtraQuestion1 != null && p.ExtraQuestion1.Contains("@"))
            {
                return true;
            }
            if (p.TVShows != null && p.TVShows.Contains("@"))
            {
                return true;
            }
            return false;
        }

        public ActionResult Report(Guid id)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }
     
            MessageSending.SendMessageToAdmin("Profile Reported", "ProfileID:" + id.ToString() + " reporter user" + WebSecurity.CurrentUserName);
            return RedirectToAction("Index", "Profile", new { flashMessage = "Profile reported.", id = id });
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
       
    }
}