﻿using LadiesChoice.Utilities;
using LadiesChoice.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LadiesChoice.Controllers
{
    public class ErrorController : Controller
    {
        public ViewResult Index()
        {
            CommonViewModel model = new CommonViewModel();
            model.ShowMenu = false;
            try
            {
                Exception ex = Server.GetLastError();
                if (ex != null)
                {
                    MessageSending.SendMessageToAdmin("500 Error", "Error" + ex.Message + " Stack trace: " + ex.StackTrace);
                }
            }
            catch { }
            return View("Error", model);
        }
        public ViewResult NotFound()
        {
            Response.StatusCode = 404;  //you may want to set this to 200
            CommonViewModel model = new CommonViewModel();
            model.ShowMenu = false;
            return View("NotFound", model);
        }

    }
}
