﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.Models;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.Security;
using WebMatrix.WebData;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using LadiesChoice.ViewModels;
using LadiesChoice.Utilities;
using LadiesChoice.Filters;

namespace LadiesChoice.Controllers
{
    [InitializeSimpleMembership]
    public class HomeController : Controller
    {
        public ActionResult Index(string flashMessage = null)
        {
            if (WebSecurity.IsAuthenticated && WebSecurity.HasUserId) {
                //return RedirectToAction("MyProfile", "Profile");
                DBEntities db = new DBEntities();
                UserProfile up = db.UserProfiles.FirstOrDefault(x => x.UserId == WebSecurity.CurrentUserId);
                if (!up.HasCompletedProfile)
                {
                    return RedirectToAction("EditProfile", "Profile");
                }
                if (!up.HasUploadedPhoto)
                {
                    return RedirectToAction("MyPhotos", "Photo");
                }
                return RedirectToAction("Inbox", "Message");
            }
            var model = new HomePageViewModel();
            model.FlashMessage = flashMessage;
            return View("index", model); 
        }

        [HttpPost]
        public ActionResult contactus(string message, string email = "")
        {
            if (WebSecurity.HasUserId)
            {
                DBEntities db = new DBEntities();
                UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserId == WebSecurity.CurrentUserId);
                email = u.Email;
                db.Dispose();
            } 
            MessageSending.SendMessageToAdmin("Contact Form Message", "From:" + email + "Message:" + message);
            return RedirectToAction("about", new { flashMessage = "Message sent!" });
        }

        public ActionResult about(string flashMessage = null)
        {
            //Utilities.CountryIPList.isIPWithinCountry("216.232.147.209");
            //String LastIPAddress = Request.Headers["Via"] ?? Request.Headers["X-Forwarded-For"];
            //string ipAddress = Request.ServerVariables["REMOTE_HOST"].ToString();
            //string ipAddress2 = Request.UserHostAddress;
            //string ipString;
            //        if (string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
            //{
            //    ipString = Request.ServerVariables["REMOTE_ADDR"];
            //}
            //else
            //{
            //    ipString = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            //        }
            //System.Diagnostics.Debug.Write(LastIPAddress);

            AboutViewModel avm;
            if (WebSecurity.HasUserId)
            {
                DBEntities db = new DBEntities();
                avm = new AboutViewModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
                db.Dispose();
            }
            else {
                avm = new AboutViewModel();
            }
            avm.FlashMessage = flashMessage;

            return View("aboutus", avm);
        }

        public ActionResult howitworks()
        {
            HowItWorksViewModel cvm = new HowItWorksViewModel(); 
            if (WebSecurity.HasUserId)
            {
                DBEntities db = new DBEntities();
                cvm.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
                db.Dispose();
            }
            
            return View("how-it-works", cvm);
        }

        public ActionResult termsandconditions()
        {
            TermsAndConditionsViewModel tacm = new TermsAndConditionsViewModel();
            if (WebSecurity.HasUserId)
            {
                DBEntities db = new DBEntities();
                tacm.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
                db.Dispose();
            }

            return View("terms-and-conditions", tacm);
        }

        public ActionResult EventDetails(int id)
        {
            DBEntities db = new DBEntities();
            EventDetailsViewModel model;
            if (WebSecurity.HasUserId)
            {
                model = new EventDetailsViewModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db, id);                
            }
            else
            {
                model = new EventDetailsViewModel(db, id);
            }
            db.Dispose();
            return View("EventDetails", model);
        }


    }
}
