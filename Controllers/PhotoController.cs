﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.Models;
using LadiesChoice.Utilities;
using LadiesChoice.ViewModels;
using System.IO;
using System.Web.Security;
using AutoMapper;
using WebMatrix.WebData;
using LadiesChoice.Filters;
using System.Configuration;
using System.Web.Helpers;
using System.Drawing.Imaging;

namespace LadiesChoice.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class PhotoController : Controller
    {
        private DBEntities db = new DBEntities();

        //
        // GET: /Photo/

        public ActionResult ViewPhotos(Guid profileModelID, string flashMessage = null) {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Photo/ViewPhotos/" + profileModelID }); }

            ProfileModel profilemodel = db.ProfileModels.FirstOrDefault(x => x.ProfileModelID == profileModelID);
            if (profilemodel == null)
            {
                return HttpNotFound();
            }
            if (profilemodel.iUserID == WebSecurity.CurrentUserId)
            { //check if the user is trying to view their own profile
                return RedirectToAction("MyPhotos");
            }

            if (db.BlockedUserModels.Count(y => y.BlockeeUserID == WebSecurity.CurrentUserId && y.BlockerUserID == profilemodel.iUserID) > 0)
            {
                return RedirectToAction("MyPhotos", new { flashMessage = "Sorry, that user has blocked you." });
            }
            
            var model = new MyPhotosViewModel(profileModelID, db);
            model.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            model.FlashMessage = flashMessage;
            return View(model);
        }
        
        public ActionResult MyPhotos(string flashMessage=null)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Photo/MyPhotos/"}); }

            var model = new MyPhotosViewModel();
            UserProfile u = db.UserProfiles.FirstOrDefault(y => y.UserId == WebSecurity.CurrentUserId);
            //model.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db, u.HasUploadedPhoto);
            model.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            model.FlashMessage = flashMessage;

            var g = WebSecurity.CurrentUserId;
            model.Photos = db.PhotoModels.Where(x => x.UserID == g ).ToList();
            model.IsIPhoneOrIpad = MobileDetectionSupport.IsMobileDevice(Request.UserAgent);
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult MyPhotos(MyPhotosViewModel model)
        {

            if (ModelState.IsValid)
            {
                Mapper.CreateMap<MyPhotosViewModel, PhotoModel>();
                PhotoModel photomodel = Mapper.Map<MyPhotosViewModel, PhotoModel>(model);

          
                var g = WebSecurity.CurrentUserId;

                photomodel.UserID = g; //set the userID otherwise it gets overwritten

                photomodel.PhotoModelID = Guid.NewGuid();
                PhotoModel p = db.PhotoModels.FirstOrDefault(x => x.UserID == g && x.IsMainPhoto==true);
                if (p == null)
                {
                    photomodel.IsMainPhoto = true;
                }
                db.PhotoModels.Add(photomodel);

                UserProfile u = db.UserProfiles.FirstOrDefault(y => y.UserId == g);
                u.HasUploadedPhoto = true;

                LadiesChoice.Utilities.ImageProcessing.resizeImage(model.File, photomodel.PhotoModelID.ToString() + ".jpg");
                db.SaveChanges();
                return RedirectToAction("MyPhotos", new { flashMessage = "Photo Uploaded." });
            }

            return View(model);
        }

        public ActionResult Editor() {
            return RedirectToAction("MyPhotos");
        }


        [HttpPost]
        public ActionResult Editor(MyPhotosViewModel model)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Photo/MyPhotos/" }); }
            
            var image = WebImage.GetImageFromRequest();
            if (image != null)
            {
                if (image.Width > 640)
                {
                    image.Resize(640, ((640 * image.Height) / image.Width));
                }
                Guid photoModelID = Guid.NewGuid();
                string filename = photoModelID.ToString() + ".jpg";

                

                image.Save(Path.Combine("~/Images", filename), ImageFormat.Jpeg.ToString());
                filename = Path.Combine("~/Images", filename);

                TagLib.File file = null;

                file = TagLib.File.Create(System.Web.HttpContext.Current.Server.MapPath(filename));
            
                var image7 = file as TagLib.Image.File;
                image7.RemoveTags(TagLib.TagTypes.AllTags);
                try
                {
                    image7.Save();
                }
                catch(Exception) {}
                double topCrop;
                double leftCrop;
                double rightCrop;
                double bottomCrop;
                double deltaCrop;
                if (image.Width > image.Height)
                {
                    deltaCrop = (image.Width - image.Height) / 2;
                    topCrop = deltaCrop;
                    leftCrop = 0;
                    rightCrop = image.Width - deltaCrop;
                    bottomCrop = image.Height;
                }
                else
                {
                    deltaCrop = (image.Height - image.Width) / 2;
                    topCrop = 0;
                    leftCrop = deltaCrop;
                    rightCrop = image.Width;
                    bottomCrop = image.Height - deltaCrop;
                }
                var editModel = new PhotoEditorViewModel()
                {
                    ImageUrl = Url.Content(filename),
                    PhotoModelID = photoModelID,
                    Width = image.Width,
                    Height = image.Height,
                    //Top = image.Height * 0.1,
                    //Left = image.Width * 0.9,
                    //Right = image.Width * 0.9,
                    //Bottom = image.Height * 0.9
                    Top = topCrop,
                    Left = leftCrop,
                    Right = rightCrop,
                    Bottom = bottomCrop,
                    IntialPageLoad = true
                };
                //UserProfile u = db.UserProfiles.FirstOrDefault(y => y.UserId == WebSecurity.CurrentUserId);

                //editModel.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db, u.HasUploadedPhoto);
                editModel.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
                //editModel.FlashMessage = "orientation" + image7.ImageTag.Orientation;
                return View("Editor", editModel);

            }

            return RedirectToAction("MyPhotos", new { flashMessage = "There was an issue uploading your photo, please try again." });
        }

        public ActionResult EditorOutput(PhotoEditorViewModel editor)
        {

            var image = new WebImage("~" + editor.ImageUrl);

            
            var height = image.Height;
            var width = image.Width;
            int cropHeight;
            int cropWidth;
            int cropTop;
            int cropLeft;
            if (editor.NoEditor)
            {
                if (width > height)
                {
                    cropWidth = (width - height) / 2;
                    cropHeight = 0;
                    cropTop = 0;
                    cropLeft = (width - height) / 2; 
                }
                else
                {
                    cropWidth = 0;
                    cropHeight = (height - width) / 2;
                    cropTop = (height - width) /2;
                    cropLeft = 0;
                }
                                
            }
            else
            {
                cropHeight = (int)(height - editor.Bottom + 1);
                cropWidth = (int)(width - editor.Right);
                if (cropHeight < 0) { cropHeight = 0; }
                if (cropWidth < 0) { cropWidth = 0; }
                cropTop = (int)editor.Top;
                cropLeft = (int)editor.Left;
                image.Crop(cropTop, cropLeft, cropHeight, cropWidth);
            }
            
            //image.Crop(cropTop, cropLeft, cropHeight, cropWidth);
            var originalFile = editor.ImageUrl;

            PhotoModel photomodel = new PhotoModel();
            photomodel.PhotoModelID = Guid.NewGuid();

            string FileNameOnly = photomodel.PhotoModelID + ".jpg";
            string tempFileName = Path.GetFileName(image.FileName);
            editor.ImageUrl = Url.Content("~/App_Data/uploads/main_" + FileNameOnly);
            //image.Save(@"~" + editor.ImageUrl);
            image.Save();

    
            AWSFunctions awsf = new AWSFunctions();
            string serverPath = System.Web.HttpContext.Current.Server.MapPath("/Images/" + tempFileName);

            awsf.UploadFile(serverPath, "main/" + FileNameOnly);

            if (editor.NoEditor) //we leave the main image uncropped if no crop tool was used; this crops the image in prep for thumbnail which must be cropped
            {
                image.Crop(cropTop, cropLeft, cropHeight, cropWidth);
            }
            image.Resize(150, 150);
            image.Save();
            awsf.UploadFile(serverPath, "thumb/" + FileNameOnly);

            var g = WebSecurity.CurrentUserId;

            photomodel.UserID = g; //set the userID otherwise it gets overwritten

            PhotoModel p = db.PhotoModels.FirstOrDefault(x => x.UserID == g && x.IsMainPhoto == true);
            if (p == null)
            {
                photomodel.IsMainPhoto = true;
            }
            db.PhotoModels.Add(photomodel);

            UserProfile u = db.UserProfiles.FirstOrDefault(y => y.UserId == g);
            u.HasUploadedPhoto = true;

            
            db.SaveChanges();
            //image.Resize(150, 150, true, false);
            //editor.ImageUrl = Url.Content("~/App_Data/uploads/thumb_" + Path.GetFileName(image.FileName));
            //image.Save(@"~" + editor.ImageUrl);

            System.IO.File.Delete(Server.MapPath(originalFile));
            return RedirectToAction("MyPhotos", new { flashMessage = "Photo Uploaded." });
        }

        public ActionResult Rotate(Guid id, bool clockWise = true) {
            string filename = id.ToString() + ".jpg";
            string path = Path.Combine("~/Images", filename);
            var image = new WebImage(path);
            if (clockWise) { image.RotateLeft(); } else { image.RotateRight(); }
            image.Save(path);
            double topCrop;
            double leftCrop;
            double rightCrop;
            double bottomCrop;
            double deltaCrop;
            if (image.Width > image.Height)
            {
                deltaCrop = (image.Width - image.Height) / 2;
                topCrop = deltaCrop;
                leftCrop = 0;
                rightCrop = image.Width - deltaCrop;
                bottomCrop = image.Height;
            }
            else
            {
                deltaCrop = (image.Height - image.Width) / 2;
                topCrop = 0;
                leftCrop = deltaCrop;
                rightCrop = image.Width;
                bottomCrop = image.Height - deltaCrop;
            }
            var editModel = new PhotoEditorViewModel()
            {
                ImageUrl = "/Images/" + filename,
                PhotoModelID = id,
                Width = image.Width,
                Height = image.Height,
                Top = topCrop,
                Left = leftCrop,
                Right = rightCrop,
                Bottom = bottomCrop
            };
            return PartialView("_photoEditor", editModel);
        }


        //
        // POST: /Photo/Delete/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Photo/MyPhotos/" }); }

            PhotoModel photomodel = db.PhotoModels.Find(id);
            if (photomodel.IsMainPhoto == true) {
                PhotoModel p = db.PhotoModels.FirstOrDefault(x => x.UserID == WebSecurity.CurrentUserId && x.PhotoModelID != id);
                if (p != null)
                {
                    p.IsMainPhoto = true;
                }
            }
            db.PhotoModels.Remove(photomodel);
            db.SaveChanges();

            string mainPhotoName = "main/" + id + ".jpg";
            string thumbPhotoName = "thumb/" + id + ".jpg";

            // store the file inside ~/App_Data/uploads folder
            //var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("PhotoPath")), mainPhotoName);
            //System.IO.File.Delete(path);
            //path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("PhotoPath")), thumbPhotoName);
            //System.IO.File.Delete(path); 
            AWSFunctions awsf = new AWSFunctions();
            awsf.DeleteFile(mainPhotoName);
            awsf.DeleteFile(thumbPhotoName);

            return RedirectToAction("MyPhotos", new { flashMessage = "Photo Deleted."});
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Caption(Guid id, string caption)
        {
            PhotoModel photomodel = db.PhotoModels.Find(id);
            photomodel.Caption = caption;
            db.SaveChanges();
            return RedirectToAction("MyPhotos", new { flashMessage = "Caption Saved."});
        }

        public ActionResult Report(Guid id, Guid photo)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }

            MessageSending.SendMessageToAdmin("Photo Reported", "ProfileID:" + id.ToString() + " reporter user:" + WebSecurity.CurrentUserName + " photo:" + ConfigurationManager.AppSettings.Get("PhotoPath") + "main/" + photo + ".jpg Profile:" + "http://ladieschoice" + ConfigurationManager.AppSettings.Get("LocalDomain") + "/Profile/Index/" + id);
            return RedirectToAction("ViewPhotos", "Photo", new { ProfileModelID = id, flashMessage = "Photo reported.  Thanks for helping improve the community!" });
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Promote(Guid id, string caption)
        {
            var g = WebSecurity.CurrentUserId;

            PhotoModel p = db.PhotoModels.FirstOrDefault(x => x.UserID == g && x.IsMainPhoto==true);
            if (p != null) {
                p.IsMainPhoto = false;
                db.SaveChanges();
            }
            PhotoModel photomodel = db.PhotoModels.Find(id);
            photomodel.IsMainPhoto = true;
            db.SaveChanges();

            return RedirectToAction("MyPhotos", new { flashMessage = "New main profile photo set." });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}