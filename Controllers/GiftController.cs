﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System.Configuration;

namespace LadiesChoice.Controllers
{
    public class GiftController : Controller
    {
        private DBEntities db = new DBEntities();
        
        //
        // GET: /Gift/

        public ActionResult Index()
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableGiftManagment"))) { return HttpNotFound(); }
            return View("Index", "_LayoutSimple", db.GiftModels.ToList());
        }

        //
        // GET: /Gift/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableGiftManagment"))) { return HttpNotFound(); }

            GiftModel giftmodel = db.GiftModels.Find(id);
            if (giftmodel == null)
            {
                return HttpNotFound();
            }
            return View("Details", "_LayoutSimple", giftmodel);
        }

        //
        // GET: /Gift/Create

        public ActionResult Create()
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableGiftManagment"))) { return HttpNotFound(); }
            return View("Create", "_LayoutSimple");
        }

        //
        // POST: /Gift/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GiftModel giftmodel)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableGiftManagment"))) { return HttpNotFound(); }
            if (ModelState.IsValid)
            {
                db.GiftModels.Add(giftmodel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Create", "_LayoutSimple", giftmodel);
        }

        //
        // GET: /Gift/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableGiftManagment"))) { return HttpNotFound(); }
            GiftModel giftmodel = db.GiftModels.Find(id);
            if (giftmodel == null)
            {
                return HttpNotFound();
            }
            return View("Edit", "_LayoutSimple", giftmodel);
        }

        //
        // POST: /Gift/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GiftModel giftmodel)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableGiftManagment"))) { return HttpNotFound(); }
            if (ModelState.IsValid)
            {
                db.Entry(giftmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Edit", "_LayoutSimple", giftmodel);
        }

        //
        // GET: /Gift/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableGiftManagment"))) { return HttpNotFound(); }
            GiftModel giftmodel = db.GiftModels.Find(id);
            if (giftmodel == null)
            {
                return HttpNotFound();
            }
            return View("Delete", "_LayoutSimple", giftmodel);
        }

        //
        // POST: /Gift/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableGiftManagment"))) { return HttpNotFound(); }
            GiftModel giftmodel = db.GiftModels.Find(id);
            db.GiftModels.Remove(giftmodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}