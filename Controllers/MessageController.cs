﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.ViewModels;
using LadiesChoice.Utilities;
using WebMatrix.WebData;
using LadiesChoice.Filters;
using AutoMapper;
using LadiesChoice.Models;
using System.Configuration;

namespace LadiesChoice.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class MessageController : Controller
    {
        //
        // GET: /Message/

        private DBEntities db = new DBEntities();


        public ActionResult Index(Guid profileModelID)
        {
            NewMessageViewModel model = new NewMessageViewModel(profileModelID, db);
            model.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Index(NewMessageViewModel newMessageViewModel) {
             
                MessageModel messageModel = new MessageModel();

                if (newMessageViewModel.Action == "Wink")
                {
                    newMessageViewModel.MessageText = "Wink! (Send her a message!)";
                    messageModel.IsWink = true;
                }
                if (newMessageViewModel.Action == "GWink")
                {
                    newMessageViewModel.MessageText = "Wink! (Send him a message!)";
                    messageModel.IsWink = true;
                }

                if (newMessageViewModel.MessageText == null || newMessageViewModel.MessageText == "")
                {
                    if (newMessageViewModel.SendingFromProfile)
                    {
                        return RedirectToAction("Index", "Profile", new { id = newMessageViewModel.ProfileModelIDOfRecipient, flashMessage = "Before you send, please enter some text for the message." });
                    }
                    else
                    {
                        return RedirectToAction("Conversation", "Message", new { id = newMessageViewModel.ProfileModelIDOfRecipient, flashMessage = "Before you send, please enter some text for the message." });
                    }
                }
                try
                {
                    messageModel.MessageModelID = Guid.NewGuid();
                    messageModel.RecipientUserID = newMessageViewModel.UserIDOfRecipient;
                    messageModel.MessageText = newMessageViewModel.MessageText;
                    messageModel.SentDate = DateTime.Now;
                    messageModel.SenderUserID = WebSecurity.CurrentUserId;
                    int creditCost = Convert.ToInt16(ConfigurationManager.AppSettings.Get("CreditCostToMessage"));

                    //check if recipient's account is disabled
                    UserProfile up = db.UserProfiles.FirstOrDefault(u => u.UserId == messageModel.RecipientUserID);
                    if (up == null)
                    {
                        return RedirectToAction("Conversation", "Message", new { id = newMessageViewModel.ProfileModelIDOfRecipient, flashMessage = "Hmm... something didn't work.  Message not sent. User may have deleted their account." });
                    }
                    bool SurpressEmailNotification = up.SurpressNewMessagesEmails;
                    if (up.IsDisabled || (db.BlockedUserModels.Count(y => y.BlockeeUserID == WebSecurity.CurrentUserId && y.BlockerUserID == up.UserId) > 0))
                    {
                        if (newMessageViewModel.SendingFromProfile)
                        {
                            return RedirectToAction("Index", "Profile", new { id = newMessageViewModel.ProfileModelIDOfRecipient, flashMessage = "Message not sent. " + up.UserName + " has disabled their account." });
                        }
                        else
                        {
                            return RedirectToAction("Conversation", "Message", new { id = newMessageViewModel.ProfileModelIDOfRecipient, flashMessage = "Message not sent. " + up.UserName + " has disabled their account." });
                        }
                    }

                    
                    //bool isMaleSender = true;
                    //int fhc = Enums.Gender.Female.GetHashCode();
                    //if (db.ProfileModels.Count(h => h.iUserID == messageModel.SenderUserID && h.Gender == fhc) > 0)
                    //{
                    //    isMaleSender = false;
                    //}
                    //if (!isMaleSender)
                    //{
                        messageModel.FilterForSpam(db);
                    //}
                    //else
                    //{
                    //    UserProfile ups = db.UserProfiles.FirstOrDefault(u => u.UserId == messageModel.SenderUserID);
                    //    int sentMessagesBefore = db.MessageModels.Count(k => k.SenderUserID == messageModel.SenderUserID && k.RecipientUserID == messageModel.RecipientUserID && messageModel.IsWink == false && k.GiftModelID == 0);
                    //    if (sentMessagesBefore == 0 && newMessageViewModel.Action != "GWink" && messageModel.RecipientUserID != int.Parse(ConfigurationManager.AppSettings.Get("AdminUserID")))
                    //    {
                    //        if (ups.NumberOfCreditsOwned < creditCost)
                    //        {
                    //            return RedirectToAction("MyAccount", "Account", new { flashMessage = "Message not sent. Please purchase some more credits." });
                    //        }

                    //        ups.NumberOfCreditsOwned = ups.NumberOfCreditsOwned - creditCost;
                    //    }
                    //}
                    db.MessageModels.Add(messageModel);

                    db.SaveChanges();

                    //send an email informing recipient of the new message
                    if (!SurpressEmailNotification)
                    {
                        if (messageModel.IsWink)
                        {
                            messageModel.SendMessage(db, WebSecurity.CurrentUserName, "You received a wink from " + WebSecurity.CurrentUserName + "!", "wink");
                        }
                        else
                        {
                            messageModel.SendMessage(db, WebSecurity.CurrentUserName);
                        }
                    }
                    string successMessageTxt = "Message Sent!";
                    if (messageModel.IsWink) { successMessageTxt = "Wink Sent!"; }
                    if (newMessageViewModel.SendingFromProfile)
                    {
                        return RedirectToAction("Index", "Profile", new { id = newMessageViewModel.ProfileModelIDOfRecipient, flashMessage = successMessageTxt });
                    }
                    else
                    {
                        return RedirectToAction("Conversation", "Message", new { id = newMessageViewModel.ProfileModelIDOfRecipient, flashMessage = successMessageTxt });
                    }
                }
                catch (Exception e)
                {
                    //if there the model was not valid
                    if (newMessageViewModel.SendingFromProfile)
                    {
                        MessageSending.SendMessageToAdmin("Message error", "An error occurred sending a message " + e.Message);
                        return RedirectToAction("Index", "Profile", new { id = newMessageViewModel.ProfileModelIDOfRecipient, flashMessage = "An error occurred.  Please try again later." });
                    }
                    else
                    {
                        MessageSending.SendMessageToAdmin("Message error", "An error occurred sending a message " + e.Message);
                        return RedirectToAction("Conversation", "Message", new { id = newMessageViewModel.ProfileModelIDOfRecipient, flashMessage = "An error occurred.  Please try again later." });
                    }
                }
        }

        public ActionResult Inbox(string flashMessage = null) {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }
            InboxViewModel inboxViewModel = new InboxViewModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            inboxViewModel.FlashMessage = flashMessage;
            return View("Conversations", inboxViewModel);
        }

        public ActionResult Conversation(Guid id, string flashMessage = null) {
            int recipUser = db.ProfileModels.FirstOrDefault(x => x.ProfileModelID == id).iUserID;
            var unreadMessages = db.MessageModels.Where(q => q.RecipientUserID == WebSecurity.CurrentUserId && q.SenderUserID == recipUser && q.Read == false);
            foreach (MessageModel urm in unreadMessages)
            {
                urm.Read = true;
            }
            db.SaveChanges();
            ConversationViewModel convoViewModel = new ConversationViewModel(WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, recipUser, db, this.HttpContext);

            convoViewModel.FlashMessage = flashMessage;
            return View("Conversation", convoViewModel);
        }

        public ActionResult Report(Guid id)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }
            
            MessageModel m = db.MessageModels.FirstOrDefault(x => x.MessageModelID == id && x.RecipientUserID == WebSecurity.CurrentUserId);
            if (m == null)
            {
                MessageSending.SendMessageToAdmin("Looks like someone reported themselves!", "messageID:" + id.ToString() + " userid" + WebSecurity.CurrentUserId);
                return RedirectToAction("Inbox", "Message", new { flashMessage = "Message reported." });
            }
            var messageText = "Reporter:" + WebSecurity.CurrentUserName + " Message Date:" + m.SentDate + " Recipient:" + m.RecipientUserID + " Sender:" + m.SenderUserID + " Text:" + m.MessageText;
            MessageSending.SendMessageToAdmin("A message has been reported!", messageText);
            return RedirectToAction("Inbox", "Message", new { flashMessage = "Message reported." });
        }

        public ActionResult Delete(Guid id)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }
            int userID = WebSecurity.CurrentUserId;
            MessageModel m = db.MessageModels.FirstOrDefault(x => x.MessageModelID == id && (x.RecipientUserID == userID || x.SenderUserID == userID ));
            
            int userIDOfConversationPartner;
            if (m == null) { return RedirectToAction("Inbox", "Message", new { flashMessage = "An error occurred trying to delete that message." }); }
            if (userID == m.SenderUserID) { 
                userIDOfConversationPartner = m.RecipientUserID;
                m.SenderHasDeleted = true;
            } else {
                userIDOfConversationPartner = m.SenderUserID;
                m.RecipientHasDeleted = true;
            }
            db.SaveChanges();
            Guid profileID = db.ProfileModels.FirstOrDefault(x => x.iUserID == userIDOfConversationPartner).ProfileModelID;

            return RedirectToAction("Conversation", new { id = profileID, flashMessage = "Message deleted." });
        }

        public ActionResult SentMessages() {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }
            SentMessagesViewModel sentMessagesViewModel = new SentMessagesViewModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            return View("SentMessages", sentMessagesViewModel);
        }

        public ActionResult ChooseGift(Guid id, bool FPP = false, string flashMessage = null)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }
            ChooseGiftViewModel model = new ChooseGiftViewModel(WebSecurity.CurrentUserId,WebSecurity.CurrentUserName,id,db, this.HttpContext);
            model.FlashMessage = flashMessage;
            model.SendingFromProfile = FPP; 
            return View(model);
        }

        [HttpPost]
        public ActionResult ChooseGift(ChooseGiftViewModel model)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }

            int giftID = model.GiftModelID;

            //if (ModelState.IsValid)
            //{
            MessageModel messageModel = new MessageModel();
            
            messageModel.GiftModelID = giftID;
            messageModel.MessageText = " ";
            messageModel.MessageModelID = Guid.NewGuid();
            messageModel.RecipientUserID = model.GiftRecipientUserID;
            messageModel.SentDate = DateTime.Now;
            messageModel.SenderUserID = WebSecurity.CurrentUserId;
            UserProfile u = db.UserProfiles.FirstOrDefault(x=>x.UserId == WebSecurity.CurrentUserId);
            GiftModel g = db.GiftModels.FirstOrDefault(y=>y.GiftModelID == giftID);
            u.NumberOfCreditsOwned = u.NumberOfCreditsOwned - g.CreditCost;
            if (u.NumberOfCreditsOwned < 0) {
                return RedirectToAction("ChooseGift", "Message", new { id = model.ProfileModelID, FPP=model.SendingFromProfile, flashMessage = "You do not have enough credits for that gift." });
            }
            db.MessageModels.Add(messageModel);    
            db.SaveChanges();
            messageModel.SendMessage(db, WebSecurity.CurrentUserName, "You received a gift from " + WebSecurity.CurrentUserName + "!", "gift");
            model.FlashMessage = "Gift sent!";
            if (model.SendingFromProfile)
            {
                return RedirectToAction("Index", "Profile", new { id = model.ProfileModelID, flashMessage = "Gift sent!" });
            }
            else {
                return RedirectToAction("Conversation", "Message", new { id = model.ProfileModelID, flashMessage = "Gift sent!" });
            }
        }
    }
}
