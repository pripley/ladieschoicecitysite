﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using LadiesChoice.Filters;
using LadiesChoice.Models;
using LadiesChoice.ViewModels;
using System.Text.RegularExpressions;
using LadiesChoice.Utilities;
using System.IO;
using System.Configuration;
using System.Data;

namespace LadiesChoice.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        private Utilities.DBEntities db = new Utilities.DBEntities();

        #region Login / Logout Methods
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            var model = new LoginViewModel();
            model.ReturnUrl = returnUrl;
            return View(model);
        }


        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                // Can login by username or email address              

                string userName = model.LoginUserName;
                //bool usernameViaEmailExists = true;

                if (model.LoginUserName.Contains("@"))
                {

                    UserProfile u = db.UserProfiles.FirstOrDefault(x => x.Email == model.LoginUserName);
                    if (u != null)
                    {
                        userName = u.UserName;
                        //if (u.IsDeleted) {
                        //    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                        //    return View(model);
                        //}
                    }
                    //else
                    //{
                    //    usernameViaEmailExists = false;
                    //}
                }
                if (db.UserProfiles.Count(x => x.UserName == userName && x.IsDeleted == true) > 0)
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    return View(model);
                }
                if (WebSecurity.Login(userName, model.Password, model.RememberMe))
                {
                    var g = WebSecurity.GetUserId(userName);
                    
                    //ProfileModel p = db.ProfileModels.FirstOrDefault(x => x.iUserID == g);
                    //Utilities.SessionHelpers.SaveGenderInSession(this.HttpContext, p.Gender);
                    UserProfile up = db.UserProfiles.FirstOrDefault(x => x.UserId == g);
                    up.LastIPAddress = getClientIPAddress();
                    up.LastLoginDate = DateTime.Now;
                    db.Entry(up).State = EntityState.Modified;
                    db.SaveChanges();
                    if (returnUrl == null)
                    {
                        if (!up.HasCompletedProfile) {
                            return RedirectToAction("EditProfile", "Profile");
                        }
                        if (!up.HasUploadedPhoto)
                        {
                            return RedirectToAction("MyPhotos", "Photo");
                        }
                        return RedirectToAction("Inbox", "Message");
                    }
                    return RedirectToLocal(returnUrl);
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }
        

        //[ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region MyAccount methods
        public virtual ActionResult MyAccount(string flashMessage = null)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Account/MyAccount" }); }
            var model = new MyAccountViewModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            model.FlashMessage = flashMessage;
            return View("MyAccount", model);
        }

        [HttpPost]
        public ActionResult SaveMailSettings(string sendNewMatches, string sendNewMessages)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Account/MyAccount" }); }
            UserProfile model = db.UserProfiles.Find(WebSecurity.CurrentUserId);
            model.SurpressReEngagementEmails = !(Convert.ToBoolean(sendNewMatches));
            model.SurpressNewMessagesEmails = !(Convert.ToBoolean(sendNewMessages));    
            db.SaveChanges();
            return RedirectToAction("MyAccount", new { flashMessage = "Your mail settings have been updated." });
        }

        //[HttpPost]
        public ActionResult BuyCredits()
        {
            SiteHandshakeModel model = new SiteHandshakeModel(WebSecurity.CurrentUserId, ConfigurationManager.AppSettings.Get("ActiveCity"));
            db.SiteHandshakeModels.Add(model);
            db.SaveChanges();
            return Redirect(ConfigurationManager.AppSettings.Get("SecureSiteDomain") + "/Payment/ProcessHandshake/?g=" + model.HandshakeKey + "&site=" + ConfigurationManager.AppSettings.Get("SiteID") + "&actionKey=1");
        }

        public ActionResult ManagePaymentOptions()
        {
            SiteHandshakeModel model = new SiteHandshakeModel(WebSecurity.CurrentUserId, ConfigurationManager.AppSettings.Get("ActiveCity"));
            db.SiteHandshakeModels.Add(model);
            db.SaveChanges();
            return Redirect(ConfigurationManager.AppSettings.Get("SecureSiteDomain") + "/Payment/ProcessHandshake/?g=" + model.HandshakeKey + "&site=" + ConfigurationManager.AppSettings.Get("SiteID") + "&actionKey=4");
        }

        [HttpPost]
        public ActionResult Subscribe()
        {
            SiteHandshakeModel model = new SiteHandshakeModel(WebSecurity.CurrentUserId, ConfigurationManager.AppSettings.Get("ActiveCity"));
            db.SiteHandshakeModels.Add(model);
            db.SaveChanges();
            return Redirect(ConfigurationManager.AppSettings.Get("SecureSiteDomain") + "/Payment/ProcessHandshake/?g=" + model.HandshakeKey + "&site=" + ConfigurationManager.AppSettings.Get("SiteID") + "&actionKey=2");
        }

        [HttpPost]
        public ActionResult ViewMySubscription()
        {
            SiteHandshakeModel model = new SiteHandshakeModel(WebSecurity.CurrentUserId, ConfigurationManager.AppSettings.Get("ActiveCity"));
            db.SiteHandshakeModels.Add(model);
            db.SaveChanges();
            return Redirect(ConfigurationManager.AppSettings.Get("SecureSiteDomain") + "/Payment/ProcessHandshake/?g=" + model.HandshakeKey + "&site=" + ConfigurationManager.AppSettings.Get("SiteID") + "&actionKey=3");
        }

        [HttpPost]
        public ActionResult Disable()
        {
            UserProfile model = db.UserProfiles.Find(WebSecurity.CurrentUserId);
            model.IsDisabled = true;
            db.SaveChanges();
            return RedirectToAction("MyAccount", new { flashMessage = "Your account has been disabled." });
        }

        [HttpPost]
        public ActionResult Enable()
        {
            UserProfile model = db.UserProfiles.Find(WebSecurity.CurrentUserId);
            model.IsDisabled = false;
            db.SaveChanges();
            return RedirectToAction("MyAccount", new { flashMessage = "Your account has been re-enabled." });
        }
        #endregion

        #region Delete Account methods
        public ActionResult Delete()
        {
            DeleteMyAccountModel model = new DeleteMyAccountModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(DeleteMyAccountModel model)
        {
            int userID = WebSecurity.CurrentUserId;
            string username = WebSecurity.CurrentUserName;
            //delete all of a users stuff
            var photos = db.PhotoModels.Where(x => x.UserID == userID);
            string thumbPhotoName;
            string mainPhotoName;
            foreach (PhotoModel photomodel in photos)
            {
                mainPhotoName = "main/" + photomodel.PhotoModelID + ".jpg";
                thumbPhotoName = "thumb/" + photomodel.PhotoModelID + ".jpg";

                // store the file inside ~/App_Data/uploads folder
                //var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("PhotoPath")), mainPhotoName);
                //System.IO.File.Delete(path);
                //path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("PhotoPath")), thumbPhotoName);
                //System.IO.File.Delete(path);
                AWSFunctions awsf = new AWSFunctions();
                awsf.DeleteFile(mainPhotoName);
                awsf.DeleteFile(thumbPhotoName);
                db.PhotoModels.Remove(photomodel);
            }
            var messages = db.MessageModels.Where(x => x.SenderUserID == userID);
            foreach (MessageModel m in messages)
            {
                m.Read = true; // in the case where the user sent a message to someone who hasn't read it (otherwise they'll have an unread message they can't read)
                db.Entry(m).State = EntityState.Modified;
            }
            MyPreferencesModel mpm = db.MyPreferencesModels.FirstOrDefault(y => y.UserID == userID);
            if (mpm != null) { db.MyPreferencesModels.Remove(mpm); }
            ProfileModel pm = db.ProfileModels.FirstOrDefault(z => z.iUserID == userID);
            if (pm != null) { db.ProfileModels.Remove(pm); }
            UserProfile u = db.UserProfiles.FirstOrDefault(v => v.UserId == userID);
            u.IsDeleted = true;
            u.IsDisabled = true;
            string email = u.Email;
            u.Email = u.UserName + "deleted_user" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Second;
            u.UserName = u.UserName + "deleted_user" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Second;
            db.SaveChanges();
            WebSecurity.Logout();
            string text = "Why: " + model.Why + " Suggestions:" + model.SuggestionsForImprovement + " email:"+ email;
            MessageSending.SendMessageToAdmin(model.site_name + ": " + username + " deleted their account", text);
            return RedirectToAction("Index", "Home", new { flashMessage = "Your profile has been deleted.  Thanks for trying us out!"});
        }
        #endregion

        #region Register methods
        [AllowAnonymous]
        public virtual ActionResult Register(string id = null)
        {
            var model = new RegisterProfileViewModel();
            model.IsFemale = true;
            //model.Gender = Enums.Gender.Female.GetHashCode();
            if (id == null)
            {
                model.Gender = -1;
                return View(model);
            }
            model.IsFemale = true;
            model.Gender = Enums.Gender.Female.GetHashCode();
            if (id.Equals("m"))
            {
                model.IsFemale = false;
                model.Gender = Enums.Gender.Male.GetHashCode();
                return View(model);
            }
            if (id.Equals("fbm"))
            {
                model.IsFemale = false;
                model.Gender = Enums.Gender.Male.GetHashCode();
                return View("RegisterMenLanding",model);
            }
            else {
                return View(model);
            }
        }



        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken()]
        public virtual ActionResult Register(RegisterProfileViewModel model)
        {
           
            if (ModelState.IsValid)
            {
                if (db.UserProfiles.Count(x => x.Email == model.Email) > 0) {
                    ModelState.AddModelError("", "This email is already in the system.  Please login.");
                    return View(model);
                }
                if (db.UserProfiles.Count(x => x.UserName == model.RequestedUserName) > 0)
                {
                    ModelState.AddModelError("", "This username is already in the system.  Please login.");
                    return View(model);
                }

                try {
                    //string ipAddress = Request.ServerVariables["REMOTE_HOST"].ToString();
                    //string ipAddress = Request.Headers["Via"] ?? Request.Headers["X-Forwarded-For"];
                    string ipAddress = getClientIPAddress();
                    bool isIPWithinCountry = false;
                    try { 
                        isIPWithinCountry = CountryIPList.isIPWithinCountry(ipAddress);
                        if (!isIPWithinCountry) {
                            MessageSending.SendMessageToAdmin("account created w ip outside of country", "User:" + model.RequestedUserName);
                        }
                    }
                    catch (Exception e){
                        MessageSending.SendMessageToAdmin("ip country lookup error", e.Message);
                    }
                    WebSecurity.CreateUserAndAccount(model.RequestedUserName, model.Password,
                                                    propertyValues: new
                                                    {
                                                        Email = model.Email,
                                                        CreationDate = DateTime.Now,
                                                        LastLoginDate = DateTime.Now,
                                                        IsSuspectedSpammer = !isIPWithinCountry,
                                                        LastIPAddress = ipAddress,
                                                        LastReEngagementEmailDate = DateTime.Now
                                                    });


                    WebSecurity.Login(model.RequestedUserName, model.Password);
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
                
                try
                {
                    ProfileModel p = new ProfileModel();


                    p.iUserID = WebSecurity.GetUserId(model.RequestedUserName);
                    
                    p.Gender = model.Gender;
                    //this.HttpContext.Session["gender"] = model.Gender;
                    p.BirthDate = (DateTime)model.BirthDate.DateTime;
                    p.Description = "";
                    p.ProfileModelID = Guid.NewGuid();
                    p.VisibleToMatches = true;
                    p.Income = -1; //so "Not Specified" comes up the first time
                    p.Education = -1; //so "Not Specified" comes up the first time
                    p.areaID = Convert.ToInt16(ConfigurationManager.AppSettings.Get("DefaultAreaOfTown"));

                    MyPreferencesModel pref = new MyPreferencesModel();
                    pref.UserID = p.iUserID;

                    MessageModel m = new MessageModel();
                    m.RecipientUserID = p.iUserID;
                    m.SenderUserID = Convert.ToInt16(ConfigurationManager.AppSettings.Get("AdminUserID"));
                    m.SentDate = DateTime.Now;
                    m.MessageModelID = Guid.NewGuid();
                    m.MessageText = "Welcome to Ladies Choice "+ model.city +"!  My name is Paul and I created this site.  If you have feedback, questions, or ideas for improvement, I'd love to hear from you.  Enjoy the site! Paul";
                    db.MessageModels.Add(m);
                    db.MyPreferencesModels.Add(pref);
                    db.ProfileModels.Add(p);
                    
                    db.SaveChanges();

                    var welcomeMessageSubjectLine = Properties.Settings.Default.WelcomeMessageSubjectLine.Replace("<username>", model.RequestedUserName);
                    welcomeMessageSubjectLine = welcomeMessageSubjectLine.Replace("<ActiveCity>", model.city);

                    string welcomeMessageText;
                    if (model.Gender == Enums.Gender.Female.GetHashCode())
                    {
                        welcomeMessageText = Properties.Settings.Default.WomenWelcomeEmailTemplate.Replace("<username>", model.RequestedUserName);
                        welcomeMessageText = welcomeMessageText.Replace("<ActiveCity>", model.city);
                        welcomeMessageText = welcomeMessageText.Replace("<Province>", model.province);
                        welcomeMessageText = welcomeMessageText.Replace("<LocalDomain>", ConfigurationManager.AppSettings.Get("LocalDomain"));
                    }
                    else
                    {
                        welcomeMessageText = Properties.Settings.Default.MenWelcomeEmailTemplate.Replace("<username>", model.RequestedUserName);
                        welcomeMessageText = welcomeMessageText.Replace("<ActiveCity>", model.city);
                        welcomeMessageText = welcomeMessageText.Replace("<Province>", model.province);
                        welcomeMessageText = welcomeMessageText.Replace("<LocalDomain>", ConfigurationManager.AppSettings.Get("LocalDomain"));
                    }
                    var response = Utilities.MessageSending.SendMessageFromLadiesChoice(model.Email, welcomeMessageSubjectLine, welcomeMessageText);
                    return RedirectToAction("EditProfile", "Profile", new { flashMessage = "Thanks for registering!  Next step is to fill out your profile.", sm = false});
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
                //end Paul's addition
            }
            //if we get here there was an error
            return View(model);
        }

        private string getClientIPAddress()
        {
            if (string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
            {
                string s = Request.ServerVariables["REMOTE_ADDR"];
                if (s.Contains(":")) {
                    s = s.Substring(0, s.IndexOf(":"));
                }
                return s;
            }
            else
            {
                string s = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (s.Contains(":")) {
                    s = s.Substring(0, s.IndexOf(":"));
                }
                return s;
            }
        }

        #endregion

        #region Change password methods

         [AllowAnonymous]
        public virtual ActionResult ForgotPassword() {
            ForgotPasswordViewModel model = new ForgotPasswordViewModel();
            if (WebSecurity.IsAuthenticated)
            {
                model.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            }
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                UserProfile u = db.UserProfiles.FirstOrDefault(x => x.Email == model.Email);
                if (u == null) {
                    ModelState.AddModelError("Email", "Email address does not exist. Please check your spelling and try again.");
                    return RedirectToAction("ForgotPassword");
                }

                // Now reset the password
                string resetToken = WebSecurity.GeneratePasswordResetToken(u.UserName);
                string subject = "[Ladies Choice" + ConfigurationManager.AppSettings.Get("ActiveCity") + "] Forgotten Password";
                string messageText = Properties.Settings.Default.ForgotPasswordTemplate.Replace("<username>", u.UserName);
                messageText = messageText.Replace("<LocalDomain>", ConfigurationManager.AppSettings.Get("LocalDomain"));
                messageText = messageText.Replace("<ActiveCity>", ConfigurationManager.AppSettings.Get("ActiveCity"));
                messageText = messageText.Replace("<passwordresetkey>", resetToken);

                MessageSending.SendMessageFromLadiesChoice(model.Email, subject, messageText);

                return RedirectToAction("Index", "Home", new { flashMessage = "Password reset.  You should receive an email on how to set your new password." });
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }
            return View(model);
        }

        [AllowAnonymous]
        public virtual ActionResult PasswordReset(string key)
        {
            PasswordResetViewModel model = new PasswordResetViewModel();
            model.Token = key;
            
            if (WebSecurity.IsAuthenticated && WebSecurity.HasUserId)
            {
                model.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public virtual ActionResult PasswordReset(PasswordResetViewModel model)
        {
            int userID = WebSecurity.GetUserIdFromPasswordResetToken(model.Token);
            // ChangePassword will throw an exception rather than return false in certain failure scenarios.
            bool changePasswordSucceeded;
            try
            {
                changePasswordSucceeded = WebSecurity.ResetPassword(model.Token, model.Password);
            }
            catch (Exception e)
            {
                changePasswordSucceeded = false;
                ModelState.AddModelError("", e.Message);
            }
            if (changePasswordSucceeded)
            {
                return RedirectToAction("Index", "Home", new { flashMessage = "Password change successful.  Please log in." });
            }
            return View(model);
        }
        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            LocalPasswordModel model = new LocalPasswordModel();
            model.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            //ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View(model);
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            if (WebSecurity.HasUserId) {
                ViewBag.ReturnUrl = Url.Action("Manage");
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
           
            return View(model);
        }
#endregion

        #region Block functions
        
        public ActionResult Block(Guid id, bool convoPage = false) 
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account", new { returnURL = "/Profile/Block/"+id }); }

            ProfileModel p = db.ProfileModels.FirstOrDefault(x => x.ProfileModelID == id);
            if (p == null)
            {
                MessageSending.SendMessageToAdmin("error trying to block user", "blocker:" + WebSecurity.CurrentUserName + " blockee:" + id);
            }
            else
            {
                BlockedUserModel b = new BlockedUserModel(p.iUserID, WebSecurity.CurrentUserId);
                db.BlockedUserModels.Add(b);
                db.SaveChanges();
            }
            if (convoPage)
            {
                return PartialView("_BlockPartial");
            }
            else
            {
                return RedirectToAction("Index", "Profile", new { id = id, flashMessage = "User blocked.  You can unblock via the My Account page." });
            }
        }

       
        public ActionResult UnBlock(int blockeeID)
        {
            BlockedUserModel b = db.BlockedUserModels.FirstOrDefault(x => x.BlockeeUserID == blockeeID && x.BlockerUserID == WebSecurity.CurrentUserId);
            db.BlockedUserModels.Remove(b);
            db.SaveChanges();
            return RedirectToAction("MyAccount", new { flashMessage = "User unblocked." });
        }

        public ActionResult BlockedUsers()
        {
            BlockedUsersViewModel b = new BlockedUsersViewModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            return View(b);
        }
        #endregion

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
