﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.Models;
using LadiesChoice.Utilities;
using AutoMapper;
using LadiesChoice.ViewModels;
using System.Web.Security;
using LadiesChoice.Filters;
using WebMatrix.WebData;

namespace LadiesChoice.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class MyPreferencesController : Controller
    {
        private DBEntities db = new DBEntities();

        //
        // GET: /MyPreferences/

        public ActionResult Index(string flashMessage = null)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }
            MyPreferencesViewModel myPreferencesViewModel = new MyPreferencesViewModel();

            //var g = (Guid)Membership.GetUser().ProviderUserKey;
            var g = WebSecurity.CurrentUserId;

            MyPreferencesModel p = db.MyPreferencesModels.FirstOrDefault(x => x.UserID == g);
            if (p == null) { //need to create a new MyPreferenceModel (probably a new account)
                p = new MyPreferencesModel();
                p.UserID = g;
                db.MyPreferencesModels.Add(p);
                db.SaveChanges();
            }
            
            Mapper.CreateMap<MyPreferencesModel, MyPreferencesViewModel>();
            myPreferencesViewModel = Mapper.Map<MyPreferencesModel, MyPreferencesViewModel>(p);

            myPreferencesViewModel.Setup(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            //myPreferencesViewModel.Username = User.Identity.Name;
            //myPreferencesViewModel.IsFemale = SessionHelpers.isFemale(this.HttpContext);

            if (myPreferencesViewModel == null)
            {
                return HttpNotFound();
            }
            myPreferencesViewModel.FlashMessage = flashMessage;
            return View(myPreferencesViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(MyPreferencesViewModel myPreferencesViewModel)
        {

            if (ModelState.IsValid)
            {
                Mapper.CreateMap<MyPreferencesViewModel, MyPreferencesModel>();
                MyPreferencesModel myPreferences = Mapper.Map<MyPreferencesViewModel, MyPreferencesModel>(myPreferencesViewModel);
                
                //var user = Membership.GetUser();
                //myPreferences.UserID = (Guid)user.ProviderUserKey; //set the userID otherwise it gets overwritten
                myPreferences.UserID = WebSecurity.CurrentUserId;

                db.Entry(myPreferences).State = EntityState.Modified;
                db.SaveChanges();

                if (myPreferencesViewModel.IsFemale) {
                    return RedirectToAction("Index", "MyMatches", new { flashMessage = "Preferences Saved." });
                }
                else {
                    return RedirectToAction("Index", "MyPreferences", new { flashMessage = "Preferences Saved." });
                }
            }

            return View(myPreferencesViewModel);
        }
        #region generatedPages
        //
        // GET: /MyPreferences/List (added by paul since I'm taking the index page)

        public ActionResult List()
        {
            return View(db.MyPreferencesModels.ToList());
        }

        //
        // GET: /MyPreferences/Details/5

        public ActionResult Details(int id = 0)
        {
            MyPreferencesModel mypreferencesmodel = db.MyPreferencesModels.Find(id);
            if (mypreferencesmodel == null)
            {
                return HttpNotFound();
            }
            return View(mypreferencesmodel);
        }

        //
        // GET: /MyPreferences/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MyPreferences/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MyPreferencesModel mypreferencesmodel)
        {
            if (ModelState.IsValid)
            {
                db.MyPreferencesModels.Add(mypreferencesmodel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mypreferencesmodel);
        }

        //
        // GET: /MyPreferences/Edit/5

        public ActionResult Edit(int id = 0)
        {
            MyPreferencesModel mypreferencesmodel = db.MyPreferencesModels.Find(id);
            if (mypreferencesmodel == null)
            {
                return HttpNotFound();
            }
            return View(mypreferencesmodel);
        }

        //
        // POST: /MyPreferences/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MyPreferencesModel mypreferencesmodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mypreferencesmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mypreferencesmodel);
        }

        //
        // GET: /MyPreferences/Delete/5

        public ActionResult Delete(int id = 0)
        {
            MyPreferencesModel mypreferencesmodel = db.MyPreferencesModels.Find(id);
            if (mypreferencesmodel == null)
            {
                return HttpNotFound();
            }
            return View(mypreferencesmodel);
        }

        //
        // POST: /MyPreferences/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MyPreferencesModel mypreferencesmodel = db.MyPreferencesModels.Find(id);
            db.MyPreferencesModels.Remove(mypreferencesmodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        #endregion
    }
}