﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System.Configuration;

namespace LadiesChoice.Controllers
{
    public class AreaOfTownController : Controller
    {
        private DBEntities db = new DBEntities();

        //
        // GET: /AreaOfTown/

        public ActionResult Index()
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableAreaOfTownManagment"))) { return HttpNotFound(); }
            return View("Index", "_LayoutSimple", db.AreaOfTownModels.ToList());
        }

        //
        // GET: /AreaOfTown/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableAreaOfTownManagment"))) { return HttpNotFound(); }
            AreaOfTownModel areaoftownmodel = db.AreaOfTownModels.Find(id);
            if (areaoftownmodel == null)
            {
                return HttpNotFound();
            }
            return View("Details", "_LayoutSimple", areaoftownmodel);
        }

        //
        // GET: /AreaOfTown/Create


        public ActionResult Create()
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableAreaOfTownManagment"))) { return HttpNotFound(); }
            return View("Create", "_LayoutSimple");
        }

        //
        // POST: /AreaOfTown/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AreaOfTownModel areaoftownmodel)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableAreaOfTownManagment"))) { return HttpNotFound(); }

            if (ModelState.IsValid)
            {
                db.AreaOfTownModels.Add(areaoftownmodel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Create", "_LayoutSimple", areaoftownmodel);
        }

        //
        // GET: /AreaOfTown/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableAreaOfTownManagment"))) { return HttpNotFound(); }

            AreaOfTownModel areaoftownmodel = db.AreaOfTownModels.Find(id);
            if (areaoftownmodel == null)
            {
                return HttpNotFound();
            }
            return View("Edit", "_LayoutSimple", areaoftownmodel);
        }

        //
        // POST: /AreaOfTown/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AreaOfTownModel areaoftownmodel)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableAreaOfTownManagment"))) { return HttpNotFound(); }

            if (ModelState.IsValid)
            {
                db.Entry(areaoftownmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Edit", "_LayoutSimple", areaoftownmodel);
        }

        //
        // GET: /AreaOfTown/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableAreaOfTownManagment"))) { return HttpNotFound(); }

            AreaOfTownModel areaoftownmodel = db.AreaOfTownModels.Find(id);
            if (areaoftownmodel == null)
            {
                return HttpNotFound();
            }
            return View("Delete", "_LayoutSimple", areaoftownmodel);
        }

        //
        // POST: /AreaOfTown/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableAreaOfTownManagment"))) { return HttpNotFound(); }

            AreaOfTownModel areaoftownmodel = db.AreaOfTownModels.Find(id);
            db.AreaOfTownModels.Remove(areaoftownmodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}