﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.ViewModels;
using AutoMapper;
using LadiesChoice.Models;
using System.Web.Security;
using LadiesChoice.Utilities;
using System.Data.Entity;
using LadiesChoice.Filters;
using WebMatrix.WebData;
using System.Data.SqlClient;
using System.Data;

namespace LadiesChoice.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class MyMatchesController : Controller
    {

        private Utilities.DBEntities db = new Utilities.DBEntities();

        //
        // GET: /MyMatches/

        
        public ActionResult Search(string flashMessage = null)
        {
            if (!WebSecurity.HasUserId) { return RedirectToAction("Login", "Account"); }
            MyMatchesViewModel myMatchesViewModel = new MyMatchesViewModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db);
            myMatchesViewModel.FlashMessage = flashMessage;
            return View("MyMatches", myMatchesViewModel);
        }

        public ActionResult Index()
        {
            MyMatchesViewModel myMatchesViewModel = new MyMatchesViewModel(this.HttpContext, WebSecurity.CurrentUserId, WebSecurity.CurrentUserName, db, true);
            return View("MyMatches", myMatchesViewModel);
        }

#region testStoredProcedure
        //public ActionResult testStoredProcedure() {
        //    SqlConnection conISWebColl = null;
        //    SqlCommand sqlCommand = null;

        //    string strRegionName = "";

        //    try
        //    {
        //        // Build DB connection
        //        //conISWebColl = new SqlConnection("Data Source=(LocalDb)\v11.0;Initial Catalog=aspnet-LadiesChoice-20130413181616;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\aspnet-LadiesChoice-20130413181616.mdf");
                
        //        // Build stored proc call
        //        //sqlCommand = new SqlCommand("getUsers", conISWebColl);
        //        sqlCommand = new SqlCommand("getUsers", conISWebColl);
                
        //        sqlCommand.CommandType = CommandType.StoredProcedure;

        //        // Add stored proc parameters
        //        //var username = sqlCommand.Parameters.Add("@strRegionName", SqlDbType.VarChar, 100);
        //        //username.Direction = ParameterDirection.Output;
        //        //sqlCommand.Parameters.Add("@strCountryCode", SqlDbType.VarChar, 100).Value = strCountryCode;
        //        //sqlCommand.Parameters.Add("@strPostalCode", SqlDbType.VarChar, 10).Value = strPostalCode;

        //        sqlCommand.Connection.Open();

        //        // Call the stored proc and get the data
        //        var v = sqlCommand.ExecuteScalar();
        //        //username = Convert.IsDBNull(username.Value) ? null : (string)username.Value;

        //    }
        //    catch (SqlException sqlEx)
        //    {
        //        throw new Exception(String.Format("An exception occurred while retrieving the Region" + " : {0}. Please contact your site administrator.", sqlEx.Message));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(String.Format("An exception occurred while retrieving the Region" + " : {0}. Please contact your site administrator.", ex.Message));
        //    }
        //    finally
        //    {
        //        conISWebColl.Close();
        //    }

        //    var model = new
        //    {
        //        site_name = "Ladies Choice Vancouver",
        //        city = "Vancouver",
        //        example_neighbourhood = "New Westminster",
        //    };
        //    return View("MyMatches");

        
        //}
#endregion
    }
}
