﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LadiesChoice.Utilities;
using LadiesChoice.Properties;
using System.Configuration;
using System.Data;

namespace LadiesChoice.Models
{
    public class MessageModel
    {
        [Key]
        public Guid MessageModelID { get; set; }
        
        public int SenderUserID { get; set; }
        public int RecipientUserID { get; set; }
        public DateTime SentDate { get; set; }
        public string MessageText { get; set; }
        public bool NeedsSpamCheck { get; set; }
        public bool Read { get; set; }
        public bool SenderHasDeleted { get; set; }
        public bool RecipientHasDeleted { get; set; }
        public bool IsWink { get; set; }
        public int GiftModelID { get; set; }

        public void SendMessage(DBEntities db, string usernameOfSender, string subjectText=null, string messageType = "message") {
            UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserId == RecipientUserID);
            string subject = Settings.Default.NewMessageSubjectLine + usernameOfSender + "!";

            if (subjectText != null)
            {
                subject = subjectText;
            }
            string city = ConfigurationManager.AppSettings.Get("ActiveCity");
            var messageText = Settings.Default.NewMessageTemplate.Replace("<usernameOfSender>", usernameOfSender);
            messageText = messageText.Replace("<usernameOfRecipient>", u.UserName);
            messageText = messageText.Replace("<ActiveCity>", city);
            messageText = messageText.Replace("{{messageType}}", messageType);

            messageText = messageText.Replace("<LocalDomain>", ConfigurationManager.AppSettings.Get("LocalDomain"));

            if (NeedsSpamCheck && !u.UserName.StartsWith("test"))
            {
                MessageSending.SendMessageToAdmin("Suspected Spam Message", messageText);
            }
            else
            {
                //var messageText = u.UserName + "you have received a message from " + usernameOfSender + ":" + MessageText.Substring(0, 50) + 
                //    "go to http://ladieschoice"+ city + ".com.  If you did not sign up for Ladies Choice " + city + "please email to cancel@ladieschoice"+city+".com" ;
                MessageSending.SendMessageFromLadiesChoice(u.Email, subject, messageText);
            }
        }
        public void FilterForSpam(DBEntities db)
        {
            
            NeedsSpamCheck = false;

            //check if this user has previously been flagged for spam
            UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserId == SenderUserID);
            if (u.IsSuspectedSpammer) { 
                NeedsSpamCheck = true;
                return;
            }
            if (AppearsToBeSpam(db, u.LastIPAddress))
            {
                u.IsSuspectedSpammer = true;
                db.Entry(u).State = EntityState.Modified;
                db.SaveChanges();
                NeedsSpamCheck = true;
            } 
        }

        private bool AppearsToBeSpam(DBEntities db, string senderIPAddress)
        {
            //if either of the 1st two messages to the recipient contain spam keywords then mark it for review
            int numPreviousMessagesSentToRecip = db.MessageModels.Count(y => y.SenderUserID == SenderUserID && y.RecipientUserID == RecipientUserID);
            if (numPreviousMessagesSentToRecip < 2)
            {
                List<string> spamList = new List<string>(Properties.Settings.Default.SPAMKeyWords.Split(',').ToList<string>());
                foreach (string spamTerm in spamList)
                {
                    if (MessageText.Contains(spamTerm))
                    {
                        // set all sent messages of this user in the last 24 hours to be reviewed 
                        DateTime yesterday = DateTime.Now.AddDays(-1);
                        IEnumerable<MessageModel> previousMessages = db.MessageModels.Where(w => w.SenderUserID == SenderUserID && w.SentDate > yesterday);

                        foreach (MessageModel m in previousMessages)
                        {
                            m.NeedsSpamCheck = true;
                            db.Entry(m).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        return true;
                    }
                }
            }

            // check the interval between this message and any previous messages sent to other recipients (if less than 1 min then flag it
            //IEnumerable<MessageModel> previousMessages = db.MessageModels.Where(w=> w.SenderUserID == SenderUserID);
            //foreach (MessageModel m in previousMessages) { 
            //    if 
            //}
            MessageModel previousMessage = db.MessageModels.Where(w => w.SenderUserID == SenderUserID).OrderBy(k => k.SentDate).FirstOrDefault();
            if (previousMessage != null)
            {
                if (DateTime.Compare(previousMessage.SentDate.AddSeconds(10), SentDate) > 0)
                {
                    return true;
                }
            }

            //string recipIP = db.UserProfiles.FirstOrDefault(x => x.UserId == RecipientUserID).LastIPAddress;
            //if (senderIPAddress.Equals(recipIP)) { return true; }
            return false;
        }
    }
}