﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LadiesChoice.Models
{
    public class MyPreferencesModel
    {
        [Key]
        public int MyPreferencesModelID { get; set; }
        public int UserID { get; set; }
        public int OlderThan { get; set; }
        public int YoungerThan { get; set; }
        public int TallerThan { get; set; }
        public int ShorterThan { get; set; }
        public int BodyType { get; set; }
        public int LookingFor { get; set; }
        public int HaveChildren { get; set; }
        public int WantChildren { get; set; }
        public int Smokes { get; set; }
        public int Drinks { get; set; }
        public int Religion { get; set; }
        public int Education { get; set; }
        public int MinimumIncome { get; set; }
        public bool ShowOnlyProfileWithPhotos { get; set; }

        public MyPreferencesModel()
        {
            Smokes = (Enums.Smokes.No | Enums.Smokes.Sometimes | Enums.Smokes.Yes).GetHashCode();
            LookingFor = (Enums.LookingFor.Relationship | Enums.LookingFor.Friends | Enums.LookingFor.Casual).GetHashCode();
            Education = (Enums.EducationLevel.HighSchool | Enums.EducationLevel.Bachelors | Enums.EducationLevel.College | Enums.EducationLevel.Masters | Enums.EducationLevel.PhD).GetHashCode();
            HaveChildren = (Enums.HaveChildren.No | Enums.HaveChildren.Yes).GetHashCode() | Enums.HaveChildren.YesDoNotLiveAtHome.GetHashCode() | Enums.HaveChildren.YesSometimesAtHome.GetHashCode();
            WantChildren = (Enums.WantChildren.Maybe | Enums.WantChildren.No | Enums.WantChildren.Yes).GetHashCode();
            Drinks = (Enums.Drinks.No | Enums.Drinks.Socially | Enums.Drinks.Yes).GetHashCode();
            Religion = (Enums.Religion.Buddist | Enums.Religion.Catholic | Enums.Religion.Hindu | Enums.Religion.Jewish | Enums.Religion.Muslim | Enums.Religion.NotReligious | Enums.Religion.Other | Enums.Religion.Protestant | Enums.Religion.Sikh | Enums.Religion.Agnostic).GetHashCode();
            OlderThan = Enums.DEFAULT_OLDER_THAN;
            YoungerThan = Enums.DEFAULT_YOUNGER_THAN;
            TallerThan = Enums.Height.Fo0.GetHashCode();
            ShorterThan = Enums.Height.Sv5.GetHashCode();
            BodyType = (Enums.BodyTypeMen.Athletic | Enums.BodyTypeMen.Average | Enums.BodyTypeMen.Slim | Enums.BodyTypeMen.MoreToLove).GetHashCode();
            MinimumIncome = Enums.MinimumIncome.zero.GetHashCode();
        }
    }
}