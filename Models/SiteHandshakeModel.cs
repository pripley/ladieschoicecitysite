﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LadiesChoice.Models
{
    public class SiteHandshakeModel
    {
        [Key]
        public int SiteHandshakeModelID { get; set; }
        
        public Guid? HandshakeKey { get; set; }
        [Required]
        public int HandshakeUserID { get; set; }
        [Required]
        public string City { get; set; }
        //public string Email { get; set; }
        //public string HandshakeUsername { get; set; }
        //public bool IsFemale { get; set; }

        public SiteHandshakeModel(int userID, string city) {
            HandshakeUserID = userID;
            City = city;
            HandshakeKey = Guid.NewGuid();
            //HandShakeDBContext hsc = new HandShakeDBContext();
            //hsc.SiteHandshakeModels.Add(this);
            //hsc.SaveChanges();
        }

        public SiteHandshakeModel() { }
    }

    //public class HandShakeDBContext : DbContext
    //{
    //    public DbSet<SiteHandshakeModel> SiteHandshakeModels { get; set; }

    //    public HandShakeDBContext()
    //        : base("SecureConnection")
    //    {
    //    }
    //}
}