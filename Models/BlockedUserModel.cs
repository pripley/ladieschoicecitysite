﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LadiesChoice.Models
{
    public class BlockedUserModel
    {
        [Key]
        public int BlockedUserModelID { get; set; } 
        public int BlockerUserID { get; set; }
        public int BlockeeUserID { get; set; }

        public BlockedUserModel(int blockeedUserID, int blockerUserID) {
            BlockerUserID = blockerUserID;
            BlockeeUserID = blockeedUserID;
        }
        public BlockedUserModel() { }
    }
}