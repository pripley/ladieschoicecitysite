﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LadiesChoice.Models
{
    public class GiftModel
    {
        [Key]
        public int GiftModelID { get; set; }
        public int CreditCost { get; set; }
        public string GiftImagePath { get; set; }
    }
}