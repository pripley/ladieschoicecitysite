﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Linq;
using LadiesChoice.ViewModels;


namespace LadiesChoice.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [RegularExpression("^([a-zA-Z0-9_.&-]+)$", ErrorMessage = "Username must be alphanumeric")]
        public string UserName { get; set; }
 
        //Pauls addition to the UserProfile model
        
        [MaxLength(100)]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public bool IsDisabled { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public bool IsSuspectedSpammer { get; set; }
        public int NumberOfCreditsOwned { get; set; }
        public bool IsDeleted { get; set; }
        public string CustomerID { get; set; }
        public DateTime SubscriptionEndDate { get; set; }
        public bool SurpressReEngagementEmails { get; set; }
        public bool SurpressNewMessagesEmails { get; set; }
        public bool IsPotentialSubscriber { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool HasCompletedProfile { get; set; }
        public bool HasUploadedPhoto { get; set; }
        public string LastIPAddress { get; set; }
        public DateTime LastReEngagementEmailDate { get; set; }
    }


    public class LocalPasswordModel : CommonViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel : CommonViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string LoginUserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        [RegularExpression("^((?!@).)*$", ErrorMessage = "User name cannot contain a @")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [EmailAddress]
        [System.ComponentModel.DataAnnotations.Compare("Email", ErrorMessage = "The emails do not match.")]
        [Display(Name = "Email Again")]
        public string EmailAgain { get; set; }

        public string msg { get; set; }
    }
}
