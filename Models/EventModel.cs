﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LadiesChoice.Models
{
    public class EventModel
    {
        [Key]
        public int EventID { get; set; }
        public string EventName { get; set; }
        public string Location { get; set; }
        public DateTime EventDate { get; set; }
        public string EventDescription { get; set; }
        public string PromoCode { get; set; }
        public string LinkToEvent { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public bool HideFromMen { get; set; }
        public bool HideFromWomen { get; set; }
        public bool Publish { get; set; }
        public int Cost { get; set; }
        public int PromoDiscount { get; set; }
        public string FormattedDate { get { return EventDate.ToString("MMM d"); } }
    }
}