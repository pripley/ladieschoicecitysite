﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace LadiesChoice.Models
{
    public class AreaOfTownModel
    {
        [Key]
        public int AreaOfTownModelID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ProfileModel> ProfileModel { get; set; } 
    }

    //public class AreaOfTownModelDBContext : DbContext
    //{
    //    public DbSet<AreaOfTownModel> AreaOfTownModels { get; set; }
    //}
}