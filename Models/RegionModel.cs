﻿using LadiesChoice.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.Models
{
    public class RegionModel
    {
        RegionModel() {

            City = Settings.Default.ActiveCity;
        }

        public string City { get; set; }
    }
}