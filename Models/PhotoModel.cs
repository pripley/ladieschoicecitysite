﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LadiesChoice.Models
{
    public class PhotoModel
    {
        [Key]
        public Guid PhotoModelID { get; set; }
        public int UserID { get; set; }
        public string Caption { get; set; }
        public bool IsMainPhoto { get; set; }
    }
}