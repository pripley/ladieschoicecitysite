﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using LadiesChoice.Utilities;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace LadiesChoice.Models
{
    public class ProfileModel
    {        
        
        
        [Key]
        public Guid ProfileModelID { get; set; }

        //public int ProfileModelID { get; set; }

        //public Guid userID { get; set; }
        public int iUserID { get; set; }
 
        [ForeignKey("areaID")]
        public virtual AreaOfTownModel area { get; set; }
        public int? areaID { get; set; }

        public string Job { get; set; }
        public int Smokes { get; set; }
        public int Drinks { get; set; }
        public int LookingFor { get; set; }
        public int Education { get; set; }
        public int Gender { get; set; }        
        public DateTime BirthDate { get; set; }
        public string Description { get; set; }
        public int Height { get; set; }
        public int BodyType { get; set; }
        public int Religion { get; set; }
        public int HaveChildren { get; set; }
        public int WantChildren { get; set; }
        public int Income { get; set; }
        public bool VisibleToMatches { get; set; }
        public bool BizzaroVisibility { get; set; }
        public string Hobbies { get; set; }
        public string ValueInAPartner { get; set; }
        public string TVShows { get; set; }
        public string WhatILikeAboutMyCity { get; set; }
        public string ExtraQuestion1 { get; set; }


        public ProfileModel() {
            LookingFor = (Enums.LookingFor.Relationship | Enums.LookingFor.Friends | Enums.LookingFor.Casual).GetHashCode();
            Smokes = Enums.Smokes.No.GetHashCode();
            Drinks = Enums.Drinks.Socially.GetHashCode();
            Height = Enums.Height.Fv8.GetHashCode();
            BodyType = Enums.BodyTypeMen.Average.GetHashCode();  //doesn't matter if it's men or women..value for average is the same
            HaveChildren = Enums.HaveChildren.No.GetHashCode();
            WantChildren = Enums.WantChildren.Maybe.GetHashCode();
            Religion = Enums.Religion.NotReligious.GetHashCode();
            Description = "";
            Income = Enums.Income.Under20K.GetHashCode();
            Job = "";
            Education = Enums.EducationLevel.HighSchool.GetHashCode();
        }
    }
}