﻿$(document).ready(function () {
    var angle = 0;
    $('#rotatebtnleft').on('click', function () {
        angle += 90;
        $("#preview").rotate(angle);
        $('#Rotation').val(angle);
    });
    $('#rotatebtnright').on('click', function () {
        angle -= 90;
        $("#preview").rotate(angle);
        $('#Rotation').val(angle);

    });
});

$(function () {
    jQuery('#profileImageEditor').Jcrop({
        onChange: showPreview,
        onSelect: showPreview,
        setSelect: [@Model.Top, @Model.Left, @Model.Right, @Model.Bottom],
        aspectRatio: 1
    });
});


function showPreview(coords, modelWidth, modelHeight)
{
    if (parseInt(coords.w) > 0)
    {
        var rx;
        var ry;
        var width = modelWidth;
        var height = modelHeight;
        var rx = 100 / coords.w;
        var ry = 100 / coords.h;
        $('#Top').val(coords.y);
        $('#Left').val(coords.x);
        $('#Bottom').val(coords.y2);
        $('#Right').val(coords.x2);
             
        if (($('#Rotation').val % 180) == 90) {
            $('#Top').val(coords.x);
            $('#Left').val(coords.y);
            $('#Bottom').val(coords.x2);
            $('#Right').val(coords.y2);
            //var rx = 100 / coords.h;
            //var ry = 100 / coords.w;
            width = modelHeight;
            height = modelWidth;
        }

        
        //var rx = 100 / coords.w;
        // var ry = 100 / coords.h;

        jQuery('#preview').css({
            width: Math.round(rx * width) + 'px',
            height: Math.round(ry * height) + 'px',
            marginLeft: '-' + Math.round(rx * coords.x) + 'px',
            marginTop: '-' + Math.round(ry * coords.y) + 'px'
        });