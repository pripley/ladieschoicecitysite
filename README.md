Site for an individual city dating site e.g., Vancouver.  The name of the city can be set in the configuration.  

Business logic at launch:
Women can see all of the men's profiles.  Women can decide if they want to show their profile only to those men they message (a la Ladies Choice Victoria) or if they would like all men to be able to see their profile (but men can only send them "winks" to show interest, but cannot send a message until a woman has sent them a message first.  Men can buy credits to use to respond to women on the site who have expressed interest.  Women can use the site for free.  

Features include:

* Users can sign up, create / edit their profile including photos (stored on AWS), message other users, send gifts, and pay for credits (via Stripe integration)

* Each city site is designed to integrate with the secure website ladieschoicesecure.com for processing payments.

* Notification emails sent via MailGun

* Ability to promote upcoming events to users (e.g., speed dating events)