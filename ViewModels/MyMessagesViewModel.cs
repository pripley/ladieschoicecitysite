﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class InboxViewModel : CommonViewModel
    {

        public IEnumerable<IndividualConversationSummaryViewModel> IndividualConversationSummaries { get; set; }
        
        public string ThumbPhotoPath { get; set; }
        public bool PromptMobileShortCut { get { return true; } }

        public InboxViewModel(HttpContextBase httpContext, int userID, string username, DBEntities db) {
            base.Setup(httpContext, userID, username, db);
            var conversationSummaries = from ur in db.UserProfiles
                                        join m in db.MessageModels on ur.UserId equals m.SenderUserID
                                        join p in db.ProfileModels on ur.UserId equals p.iUserID 

                                        join ph in db.PhotoModels on ur.UserId equals ph.UserID into joined
                                        from j in joined.DefaultIfEmpty()
                                        join g1 in db.GiftModels on m.GiftModelID equals g1.GiftModelID into gjoined
                                        from k in gjoined.DefaultIfEmpty()
                                        where (j.IsMainPhoto == true || j.IsMainPhoto == null) && 
                                        m.NeedsSpamCheck == false &&
                                        m.RecipientUserID == userID && m.SenderUserID == ur.UserId && m.RecipientHasDeleted == false
                                        group new { m, ur, j, p, k } by new { m.SenderUserID, ur.UserId, ur.UserName, j.PhotoModelID } into g
                                        //group n by n.AccountId into g
                                        // select g.OrderByDescending(t=>t.Date).First();
                                        //orderby g.OrderByDescending(t => t.m.SentDate)
                                        select new IndividualConversationSummaryViewModel
                                        {
                                            LastMessageSentDateUTC = g.OrderByDescending(t => t.m.SentDate).FirstOrDefault().m.SentDate,
                                            MessageText = g.OrderByDescending(t => t.m.SentDate).FirstOrDefault().m.MessageText,
                                            PhotoModelID = g.OrderByDescending(t => t.m.SentDate).FirstOrDefault().j.PhotoModelID,
                                            UsernameOfConversationPartner = g.OrderByDescending(t => t.m.SentDate).FirstOrDefault().ur.UserName,
                                            UserIDOfConversationPartner = g.OrderByDescending(t => t.m.SentDate).FirstOrDefault().ur.UserId,
                                            ProfileModelID = g.OrderByDescending(t => t.m.SentDate).FirstOrDefault().p.ProfileModelID,
                                            GiftImagePath = g.OrderByDescending(t => t.m.SentDate).FirstOrDefault().k.GiftImagePath
                                        };

      
            this.IndividualConversationSummaries = conversationSummaries.ToList();
            IndividualConversationSummaries = IndividualConversationSummaries.OrderByDescending(t => t.LastMessageSentDate);
            //IndividualConversationSummaries.OrderByDescending(k => k.LastMessageSentDate);
            this.ThumbPhotoPath = ConfigurationManager.AppSettings.Get("PhotoPath") + "thumb/";
        } 

    }
}