﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LadiesChoice.ViewModels
{
    public class EditProfileViewModel : CommonViewModel
    {
        public EditProfileViewModel()
        {
            BindData();
        }
       
        public Guid ProfileModelID { get; set; }
        public UserProfile EmailAndUsernameUserProfile { get; set; }
        public bool ShowFBCode { get { return true; } }

        [Display(Name = "Birth Date")]
        [DateValidation("Please choose a valid Birth Date.")]
        public Date BirthDateComposite { get; set; }

        [Required]
        [Display(Name = "Area of Town")]
        public int? areaID { get; set; }

        public virtual AreaOfTownModel area { get; set; }

        [Required]
        public string Job { get; set; }

        public bool VisibleToMatches { get; set; }
        public bool BizzaroVisibility { get; set; }

        //[Required]
        [Display(Name = "In my spare time I enjoy...")]
        //[StringLength(9999999, MinimumLength = 50, ErrorMessage = "Please write more about what you like to do in your spare time")]
        public string Hobbies { get; set; }
        [Display(Name = "Things that are important to me include...")]
        public string ValueInAPartner { get; set; }

        [Required]
        [Display(Name = "About me...")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [StringLength(9999999,MinimumLength = 50, ErrorMessage="Please write more about yourself in the About Me field")]
        public string Description { get; set; }

        [Display(Name = "Books, TV shows, movies, music I like include...")]
        public string TVShows { get; set; }
        
        public string WhatILikeAboutMyCity { get; set; }
        
        [Display(Name = "You should contact me if...")]
        public string ExtraQuestion1 { get; set; }

        public int Smokes { get; set; }
        public IEnumerable<SelectListItem> SmokesList { get; protected set; }

        public int Drinks { get; set; }
        public IEnumerable<SelectListItem> DrinksList { get; protected set; }

        [Display(Name = "Looking For")]
        public Enums.LookingFor LookingFor { get; set; }


        public int Education { get; set; }
        public IEnumerable<SelectListItem> EducationList { get; protected set; }

        public int Gender { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; protected set; }

        public int Height { get; set; }
        public IEnumerable<SelectListItem> HeightList { get; protected set; }

        [Display(Name = "Body Type")]
        public int BodyType { get; set; }
        public IEnumerable<SelectListItem> BodyTypeList { get; protected set; }

        public int Religion { get; set; }
        public IEnumerable<SelectListItem> ReligionList { get; protected set; }

        [Display(Name = "Have Kids")]
        public int HaveChildren { get; set; }
        public IEnumerable<SelectListItem> HaveChildrenList { get; protected set; }

        [Display(Name = "Want (More) Kids")]
        public int WantChildren { get; set; }
        public IEnumerable<SelectListItem> WantChildrenList { get; protected set; }

        [Required(ErrorMessage = "Please specify your income.  Used only for matching and won't show on your profile.")]
        public int Income { get; set; }
        public IEnumerable<SelectListItem> IncomeList { get; protected set; }

        public int Year { get; set; }
        public IEnumerable<SelectListItem> YearList { get; protected set; }
        public int Month { get; set; }
        public IEnumerable<SelectListItem> MonthList { get; protected set; }
        public int Day { get; set; }
        public IEnumerable<SelectListItem> DayList { get; protected set; }
 
        private void BindData()
        {

            GenderList = Enum.GetValues(typeof(Enums.Gender)).Cast<Enums.Gender>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            });

            SmokesList = Enum.GetValues(typeof(Enums.Smokes)).Cast<Enums.Smokes>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            });

            DrinksList = Enum.GetValues(typeof(Enums.Drinks)).Cast<Enums.Drinks>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            });

          
            EducationList = EnumExtensions.ToSelectList<Enums.EducationLevel>((Enums.EducationLevel)this.Education);
            ReligionList = EnumExtensions.ToSelectList<Enums.Religion>((Enums.Religion)this.Religion);


            WantChildrenList = Enum.GetValues(typeof(Enums.WantChildren)).Cast<Enums.WantChildren>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            });

            
            HaveChildrenList = EnumExtensions.ToSelectList<Enums.HaveChildren>((Enums.HaveChildren)this.HaveChildren);
            HeightList = EnumExtensions.ToSelectList<Enums.Height>((Enums.Height)this.Height);
            IncomeList = EnumExtensions.ToSelectList<Enums.Income>((Enums.Income)this.Income);

            var now = DateTime.Now;
            YearList = Enumerable.Range(0, 100).Select(x => new SelectListItem { Value = (now.Year - 18 - x).ToString(), Text = (now.Year - 18 - x).ToString() });
            MonthList = Enumerable.Range(1, 12).Select(x => new SelectListItem { Text = new DateTime(now.Year, x, 1).ToString("MMMM"), Value = x.ToString() });
            DayList = Enumerable.Range(1, 31).Select(x => new SelectListItem { Value = x.ToString(), Text = x.ToString() });
            BirthDateComposite = new Date();
        }

        //used to set whether the BodyTypeList is the male or female version.  Needs to be run after the modelview has been populated with the profilemodel so that we know whether the gender is male or female
        public void setBodyTypeList() {
         if (this.Gender == Enums.Gender.Male.GetHashCode())
            {         
                BodyTypeList = EnumExtensions.ToSelectList<Enums.BodyTypeMen>((Enums.BodyTypeMen)this.BodyType);
            }
            else
            {   
                BodyTypeList = EnumExtensions.ToSelectList<Enums.BodyTypeWomen>((Enums.BodyTypeWomen)this.BodyType);
            }
        }

        public void RebindData()
        {
            BindData();
        }


    }
}