﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LadiesChoice.ViewModels
{
    public class MyPreferencesViewModel : CommonViewModel
    {
        public int MyPreferencesModelID { get; set; }
        [Display(Name = "Older Than")]
        public int OlderThan { get; set; }
        [Display(Name = "Younger Than")]
        public int YoungerThan { get; set; }
        [Display(Name = "Taller Than")]
        public int TallerThan { get; set; }
        [Display(Name = "Shorter Than")]
        public int ShorterThan { get; set; }
        [Display(Name = "Body Type")]
        public Enums.BodyTypeMen BodyType { get; set; }
        [Display(Name = "Looking For")]
        public Enums.LookingFor LookingFor { get; set; }
        [Display(Name = "Has Children")]
        public Enums.HaveChildren HaveChildren { get; set; }
        [Display(Name = "Wants Children")]
        public Enums.WantChildren WantChildren { get; set; }
        public Enums.Smokes Smokes { get; set; }
        public Enums.Drinks Drinks { get; set; }
        public Enums.Religion Religion { get; set; }
        [Display(Name = "Education")]
        public Enums.EducationLevel Education { get; set; }
        [Display(Name = "Minimum Income")]
        public int MinimumIncome { get; set; }
        [Display(Name = "Only show profiles with photos")]
        public bool ShowOnlyProfileWithPhotos { get; set; }


        public IEnumerable<SelectListItem> OlderThanList { get; protected set; }
        public IEnumerable<SelectListItem> YoungerThanList { get; protected set; }
        public IEnumerable<SelectListItem> TallerThanList { get; protected set; }
        public IEnumerable<SelectListItem> ShorterThanList { get; protected set; }
        public IEnumerable<SelectListItem> MinimumIncomeList { get; protected set; }


        public MyPreferencesViewModel() {
            BindData();
        }

        private void BindData()
        {
            OlderThanList = Enumerable.Range(18, 99).Select(x => new SelectListItem { Value = x.ToString(), Text = x.ToString() });
            YoungerThanList = Enumerable.Range(18, 99).Select(x => new SelectListItem { Value = x.ToString(), Text = x.ToString() });
            TallerThanList = EnumExtensions.ToSelectList<Enums.Height>((Enums.Height)this.TallerThan);
            ShorterThanList = EnumExtensions.ToSelectList<Enums.Height>((Enums.Height)this.ShorterThan);
            MinimumIncomeList = EnumExtensions.ToSelectList<Enums.MinimumIncome>((Enums.MinimumIncome)this.MinimumIncome);
        }

        public void RebindData()
        {
            BindData();
        }
    }
}