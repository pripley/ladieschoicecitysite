﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class MyPhotosViewModel : CommonViewModel
    {
        
        public Guid PhotoModelID;
        public string PhotoPath { get { return ConfigurationManager.AppSettings.Get("PhotoPath"); } }
        //[Required]
        //[DataType(DataType.Upload)]

        [Display(Name = "Add a photo")]
        [ValidateFile(ErrorMessage = "Please select a JPG or PNG image smaller than 3MB")]
        public HttpPostedFileBase File { get; set; }
        public string Caption { get; set; }

        public IEnumerable<Models.PhotoModel> Photos { get; set; }
       
        public string UsernameOfPhotoOwner { get; set; }

        public bool IsIPhoneOrIpad { get; set; }
        public Guid ProfileModelID { get; set; }
        public MyPhotosViewModel() {
            
        }

        public MyPhotosViewModel(Guid profileModelID, DBEntities db) {
            
            var userInfo = from pf in db.ProfileModels
                              join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                              where pf.ProfileModelID == profileModelID
                              select new
                              {
                                  Userid = ur.UserId,
                                  Username = ur.UserName,                                 
                              }
                  ;
            var p = userInfo.FirstOrDefault();
            this.UsernameOfPhotoOwner = p.Username;
            this.Photos = db.PhotoModels.Where(x => x.UserID == p.Userid).ToList();
            ProfileModelID = profileModelID;
        }
    }
}