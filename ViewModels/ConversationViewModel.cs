﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System.Configuration;

namespace LadiesChoice.ViewModels
{
    public class ConversationViewModel : CommonViewModel
    {
        public IEnumerable<IndividualMessageViewModel> Messages { get; set; }
        public string ThumbPhotoPath { get; set; }
        public string UsernameOfConversationPartner { get; set; }
        public int UserIDOfConversationPartner { get; set; }
        public bool HasConversationPartnerDisabledTheirProfile { get; set; }
        public bool HasConversationPartnerBlockedViewer { get; set; }
        public bool CanSend { get; set; }
        public bool HasSentBefore { get; set; }        
        public bool HasReceivedBefore { get; set; }
        public Guid ProfileIDOfConversationPartner { get; set; }
        public bool IsConversationPartnerAdminUser { get; set; }

        public int CreditCostToMessage { get { return Convert.ToInt16(ConfigurationManager.AppSettings.Get("CreditCostToMessage")); } }

        public ConversationViewModel(int userID, string username, int userIDOfConversationPartner, DBEntities db, HttpContextBase httpContext)        
        {
            base.Setup(httpContext, userID, username, db);
            var conversation = from ur in db.UserProfiles
                           join m in db.MessageModels on ur.UserId equals m.SenderUserID
                           join p in db.ProfileModels on ur.UserId equals p.iUserID into pjoined
                               from pj in pjoined.DefaultIfEmpty()
                           join ph in db.PhotoModels on ur.UserId equals ph.UserID into joined
                               from j in joined.DefaultIfEmpty()
                           join g in db.GiftModels on m.GiftModelID equals g.GiftModelID into gjoined
                               from k in gjoined.DefaultIfEmpty()
                               where m.NeedsSpamCheck == false && (j.IsMainPhoto == true || j.IsMainPhoto == null)  && 
                           ((m.SenderUserID == userID && m.RecipientUserID == userIDOfConversationPartner) ||
                           (m.RecipientUserID == userID && m.SenderUserID == userIDOfConversationPartner))
                            orderby m.SentDate ascending
                               select new IndividualMessageViewModel()
                           {
                               SentDateUTC = m.SentDate,
                               PhotoModelID = j.PhotoModelID,
                               UsernameOfMessageSender = ur.UserName,
                               UserIDOfMessageSender = ur.UserId,
                               MessageText = m.MessageText,
                               Read = m.Read,
                               ProfileModelID = pj.ProfileModelID,
                               MessageModelID = m.MessageModelID,
                               IsWink = m.IsWink,
                               GiftImagePath = k.GiftImagePath,
                               SenderDeleted = m.SenderHasDeleted,
                               RecipientDeleted = m.RecipientHasDeleted
                           };

            this.Messages = conversation.ToList<IndividualMessageViewModel>();
            this.ThumbPhotoPath = ConfigurationManager.AppSettings.Get("PhotoPath") + "thumb/";
            UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserId == userIDOfConversationPartner);
            this.UsernameOfConversationPartner = u.UserName;
            this.UserIDOfConversationPartner = u.UserId;
            ProfileIDOfConversationPartner = db.ProfileModels.FirstOrDefault(x => x.iUserID == userIDOfConversationPartner).ProfileModelID;
            this.HasConversationPartnerDisabledTheirProfile = u.IsDisabled;
            if (db.BlockedUserModels.Count(y => y.BlockerUserID == userIDOfConversationPartner && y.BlockeeUserID == userID) > 0) {
                HasConversationPartnerBlockedViewer = true;
            }

            if (!IsFemale)
            {
                u = db.UserProfiles.FirstOrDefault(z => z.UserId == userID);
                HasSentBefore = false;
                int numSentMessages = Messages.Count(k => k.UserIDOfMessageSender == userID  && k.IsWink == false && k.GiftImagePath == null);
                if (numSentMessages > 0)
                {
                    HasSentBefore = true;
                }
                if (Messages.Count() - Messages.Count(k => k.UserIDOfMessageSender == userID) > 0)
                {
                    HasReceivedBefore = true;
                }
                if (CreditCostToMessage <= u.NumberOfCreditsOwned || HasSentBefore)
                {
                    this.CanSend = true;
                }
            }
            else {
                CanSend = true;
                HasSentBefore = true;
            }
            if (Convert.ToInt16(ConfigurationManager.AppSettings.Get("AdminUserID")) == userIDOfConversationPartner)
            {
                IsConversationPartnerAdminUser = true;
            }
            else {
                IsConversationPartnerAdminUser = false;
            }

        }
    }
}