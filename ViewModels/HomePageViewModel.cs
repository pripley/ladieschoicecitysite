﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class HomePageViewModel : CommonViewModel
    {
        public string example_neighbourhood { get { return ConfigurationManager.AppSettings.Get("ExampleNeighbourhood"); } }
        public string PhotoPath { get { return ConfigurationManager.AppSettings.Get("PhotoPath"); } }
        public HomePageViewModel() {
            base.user = false;
        }
    }
}