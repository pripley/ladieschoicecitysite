﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class HowItWorksViewModel : CommonViewModel
    {
        public string PhotoPath { get { return ConfigurationManager.AppSettings.Get("PhotoPath"); } }

        public HowItWorksViewModel(HttpContextBase httpContext, int userID, string username, DBEntities db) {
            base.Setup(httpContext, userID, username, db);
        }
        public HowItWorksViewModel()
        {
        }
    }
}