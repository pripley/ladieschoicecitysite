﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LadiesChoice.Models;
using System.ComponentModel.DataAnnotations;
using LadiesChoice.Utilities;
using System.Configuration;

namespace LadiesChoice.ViewModels
{
    public class RegisterProfileViewModel : CommonViewModel
    {
        public RegisterProfileViewModel()
        {
            BindData();
        }

        public string PhotoPath { get { return ConfigurationManager.AppSettings.Get("PhotoPath"); } }

        [Required]
        [Display(Name = "User name")]
        [RegularExpression("^([a-zA-Z0-9_.&-]+)$", ErrorMessage = "Username must be alphanumeric")]
        public string RequestedUserName { get; set; }

        public int Gender { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; protected set; }

        //[Required]
        [Display(Name = "Birth Date")]
        [DateValidation("Please choose a valid Birth Date.")]
        public Utilities.Date BirthDate { get; set; }

        public int Year { get; set; }
        public IEnumerable<SelectListItem> YearList { get; protected set; }
        public int Month { get; set; }
        public IEnumerable<SelectListItem> MonthList { get; protected set; }
        public int Day { get; set; }
        public IEnumerable<SelectListItem> DayList { get; protected set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [EmailAddressAttribute]
        public string Email { get; set; }

        [EmailAddress]
        [System.ComponentModel.DataAnnotations.Compare("Email", ErrorMessage = "The emails do not match.")]
        [Display(Name = "Email Again")]
        public string EmailAgain { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        private void BindData()
        {
            GenderList = Enum.GetValues(typeof(Enums.Gender)).Cast<Enums.Gender>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            });
            var now = DateTime.Now;
            YearList = Enumerable.Range(0, 90).Select(x => new SelectListItem { Value = (now.Year - 18 - x).ToString(), Text = (now.Year - 18 - x).ToString() });
            MonthList = Enumerable.Range(1, 12).Select(x => new SelectListItem { Text = new DateTime(now.Year, x, 1).ToString("MMM"), Value = x.ToString() });
            DayList = Enumerable.Range(1, 31).Select(x => new SelectListItem { Value = x.ToString(), Text = x.ToString() });
            BirthDate = new Date();
        }

        public void RebindData()
        {
            BindData();
        }

    }
}
