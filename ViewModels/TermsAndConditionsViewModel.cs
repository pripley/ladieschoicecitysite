﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class TermsAndConditionsViewModel : CommonViewModel
    {
        public string LocalDomain { get { return ConfigurationManager.AppSettings.Get("LocalDomain"); } }

        public TermsAndConditionsViewModel(HttpContextBase httpContext, int userID, string username, DBEntities db) {
            base.Setup(httpContext, userID, username, db);
        }
        public TermsAndConditionsViewModel()
        {
        }
    }
}