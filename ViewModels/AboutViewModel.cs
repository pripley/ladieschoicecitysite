﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class AboutViewModel : CommonViewModel
    {
        public string DateFounded { get { return ConfigurationManager.AppSettings.Get("DateFounded"); } }

        public AboutViewModel(HttpContextBase httpContext, int userID, string username, DBEntities db) {
            base.Setup(httpContext, userID, username, db);
        }
        public AboutViewModel()
        {
        }
    }
}