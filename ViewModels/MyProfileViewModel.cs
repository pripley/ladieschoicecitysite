﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class MyProfileViewModel : CommonViewModel
    {
        public Guid ProfileModelID { get; set; }
        public int UserIDOfProfile { get; set; }
        public string UsernameOfProfile { get; set; }
        public string area { get; set; }
        public string Job { get; set; }
        public string Smokes { get; set; }
        public string Drinks { get; set; }
        public string LookingFor { get; set; }
        public string Education { get; set; }
        public string Age { get; set; }
        public string Description { get; set; }
        public string Hobbies { get; set; }
        public string ValueInAPartner { get; set; }
        public string TVShows { get; set; }
        public string WhatILikeAboutMyCity { get; set; }
        public string ExtraQuestion1 { get; set; }
        public string Height { get; set; }
        public string BodyType { get; set; }
        public string Religion { get; set; }
        public string HaveChildren { get; set; }
        public string WantChildren { get; set; }
        public string Income { get; set; }
        public bool HasUserDisabledTheirAccount { get; set; }
        public int Gender { get; set; }

        public string MainPhoto { get; set; }
        public int NumMorePhotos { get; set; }
        public Guid? PhotoModelID { get; set; }
        public int iSmokes { get; set; }
        public int iDrinks { get; set; }
        public int iLookingFor { get; set; }
        public int iEducation { get; set; }
        public int iHeight { get; set; }
        public int iBodyType { get; set; }
        public int iReligion { get; set; }
        public int iHaveChildren { get; set; }
        public int iWantChildren { get; set; }
        public int iIncome { get; set; }
        public DateTime dtBirthDate { get; set; }

        public bool HasBlockedViewer { get; set; }
        
        public MyProfileViewModel(HttpContextBase httpContext, int userID, string username, DBEntities db)
        {
            Setup(httpContext, userID, username, db);
            var profileView = from pf in db.ProfileModels
                              join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                              join ph in db.PhotoModels on pf.iUserID equals ph.UserID into joined
                              from j in joined.DefaultIfEmpty()
                              join ar in db.AreaOfTownModels on pf.areaID equals ar.AreaOfTownModelID into joined2
                              from z in joined2.DefaultIfEmpty()
                              where pf.iUserID == userID
                              orderby j.IsMainPhoto descending

                              select new IndividualProfileViewModel
                              {

                                  area = z.Name,
                                  Job = pf.Job,
                                  Description = pf.Description,
                                  Hobbies = pf.Hobbies,
                                  ValueInAPartner = pf.ValueInAPartner,
                                  TVShows = pf.TVShows,
                                  ExtraQuestion1 = pf.ExtraQuestion1,
                                  WhatILikeAboutMyCity = pf.WhatILikeAboutMyCity,
                                  PhotoModelID = (Guid?)j.PhotoModelID,
                                  iReligion = pf.Religion,
                                  iDrinks = pf.Drinks,
                                  iEducation = pf.Education,
                                  iBodyType = pf.BodyType,
                                  iHaveChildren = pf.HaveChildren,
                                  iWantChildren = pf.WantChildren,
                                  iHeight = pf.Height,
                                  iIncome = pf.Income,
                                  iSmokes = pf.Smokes,
                                  dtBirthDate = pf.BirthDate,
                                  iLookingFor = pf.LookingFor,
                                  Gender = pf.Gender,
                                  UsernameOfProfile = ur.UserName,
                                  ProfileModelID = pf.ProfileModelID,
                                  UserIDOfProfile = ur.UserId,
                                  HasUserDisabledTheirAccount = ur.IsDisabled
                              }
                              ;
            IndividualProfileViewModel p = profileView.FirstOrDefault();
            int numPhotos = 0;
            if (p.PhotoModelID != null)
            {
                numPhotos = profileView.Count();
                this.NumMorePhotos = numPhotos - 1;
                this.MainPhoto = ConfigurationManager.AppSettings.Get("PhotoPath") + "thumb/" + p.PhotoModelID + ".jpg";
            }
            else
            {
                this.MainPhoto = "/Content/images/placeholder.png";
            }
            this.HasUserDisabledTheirAccount = p.HasUserDisabledTheirAccount;

            this.UserIDOfProfile = p.UserIDOfProfile;
            this.ProfileModelID = p.ProfileModelID;
            this.Job = p.Job;
            this.area = p.area;
            this.Gender = p.Gender;
            this.UsernameOfProfile = p.UsernameOfProfile;
            if (p.Description != null) { this.Description = p.Description.Replace("\n", "<br>"); }
            if (p.Hobbies != null) { this.Hobbies = p.Hobbies.Replace("\n", "<br>"); }
            if (p.ValueInAPartner != null) { this.ValueInAPartner = p.ValueInAPartner.Replace("\n", "<br>"); }
            if (p.TVShows != null) { this.TVShows = p.TVShows.Replace("\n", "<br>"); }
            if (p.ExtraQuestion1 != null) { this.ExtraQuestion1 = p.ExtraQuestion1.Replace("\n", "<br>"); }
            if (p.WhatILikeAboutMyCity != null) { this.WhatILikeAboutMyCity = p.WhatILikeAboutMyCity.Replace("\n", "<br>"); }

            this.Education = EnumExtensions.EnumValueToDisplayString<Enums.EducationLevel>(p.iEducation);
            this.Drinks = EnumExtensions.EnumValueToDisplayString<Enums.Drinks>(p.iDrinks);
            this.Smokes = EnumExtensions.EnumValueToDisplayString<Enums.Smokes>(p.iSmokes);
            this.Income = EnumExtensions.EnumValueToDisplayString<Enums.Income>(p.iIncome);
            this.Height = EnumExtensions.EnumValueToDisplayString<Enums.Height>(p.iHeight);
            this.Religion = EnumExtensions.EnumValueToDisplayString<Enums.Religion>(p.iReligion);
            this.Age = AgeHelper.getAgeToday(p.dtBirthDate).ToString();
            this.LookingFor = EnumExtensions.EnumMultiValuesToDisplayString<Enums.LookingFor>(p.iLookingFor);
            this.HaveChildren = EnumExtensions.EnumValueToDisplayString<Enums.HaveChildren>(p.iHaveChildren);
            this.WantChildren = EnumExtensions.EnumValueToDisplayString<Enums.WantChildren>(p.iWantChildren);
            if (IsFemale)
            {
                this.BodyType = EnumExtensions.EnumValueToDisplayString<Enums.BodyTypeWomen>(p.iBodyType);
            }
            else
            {
                this.BodyType = EnumExtensions.EnumValueToDisplayString<Enums.BodyTypeMen>(p.iBodyType);
            }
        }
    }
}