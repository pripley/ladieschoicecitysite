﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class PhotoEditorViewModel : CommonViewModel
    {

        public string ImageUrl { get; set; }
        public Guid PhotoModelID { get; set; }
        public double Top { get; set; }
        public double Bottom { get; set; }
        public double Left { get; set; }
        public double Right { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public bool NoEditor { get; set; }
        public bool IntialPageLoad { get; set; }
    }
}