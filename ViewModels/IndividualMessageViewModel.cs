﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class IndividualMessageViewModel
    {
        public DateTime SentDate { get { return Utilities.TimeZoneSupport.GetLocalTime(SentDateUTC); } }
        public string UsernameOfMessageSender { get; set; }
        public Guid? PhotoModelID { get; set; }
        public int UserIDOfMessageSender { get; set; }
        public string MessageText { get; set; }
        public bool Read { get; set; }
        public Guid? ProfileModelID { get; set; }
        public Guid MessageModelID { get; set; }
        public bool IsWink { get; set; }
        public string GiftImagePath { get; set; }
        public bool SenderDeleted { get; set; }
        public bool RecipientDeleted { get; set; }
        public DateTime SentDateUTC { get; set; }
    }
}