﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class MyAccountViewModel : CommonViewModel
    {
        public bool Disabled { get; set; }
        public int NumberOfCreditsOwned { get; set; }
        public int SiteID { get { return Convert.ToInt32(ConfigurationManager.AppSettings.Get("SiteID")); } }
        public bool IsSubscriber { get; set; }
        public bool IsPotentialSubscriber { get; set; }
        public bool SurpressReEngagementEmails { get; set; }
        public bool SurpressNewMessageEmails { get; set; }
        public bool HasSavedCard { get; set; }
        
        public MyAccountViewModel(HttpContextBase httpContext, int userID, string UserName, DBEntities db) 
        {
            base.Setup(httpContext, userID, UserName, db);
            UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserId == userID);
            Disabled = u.IsDisabled;
            NumberOfCreditsOwned = u.NumberOfCreditsOwned;
            if (u.CustomerID != null) { HasSavedCard = true; }
            IsPotentialSubscriber = u.IsPotentialSubscriber;
            SurpressReEngagementEmails = u.SurpressReEngagementEmails;
            SurpressNewMessageEmails = u.SurpressNewMessagesEmails;
            if (u.CustomerID != null && IsFemale && u.SubscriptionEndDate != DateTime.MinValue) { IsSubscriber = true; }
        }


    }
}