﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class NewMessageViewModel : CommonViewModel
    {
        public string UsernameOfReceipient { get; set; }
        public int UserIDOfRecipient { get; set; }
        public int UserID { get; set; }
        public int Parent { get { return 0; } }
        public string Thumb { get; set; }       
        public string Height { get; set; }
        public string Age { get; set; }
        public string Area { get; set; }
        public Guid ProfileModelID { get; set; }

        [Required]
        public string MessageText { get; set; }
        public string Action { get; set; }
        //public string SecurityToken { get; set; }
        public bool SendingFromProfile { get; set; }
        public Guid ProfileModelIDOfRecipient { get; set; }
        //public int GiftModelID { get; set; }

        public NewMessageViewModel() { }

        public NewMessageViewModel(Guid profileModelID, DBEntities db)
        {

            var profileCard = from pf in db.ProfileModels
                              join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                              join ph in db.PhotoModels on pf.iUserID equals ph.UserID into joined
                              from j in joined.DefaultIfEmpty()
                              join ar in db.AreaOfTownModels on pf.areaID equals ar.AreaOfTownModelID into joined2
                              from z in joined2.DefaultIfEmpty()
                              where pf.ProfileModelID == profileModelID
                          
                              select new
                              {
                                  area = z.Name,
                                  PhotoModelID = (Guid?)j.PhotoModelID,
                                  Height = pf.Height,
                                  dtBirthDate = pf.BirthDate,
                                  RecipientUsername = ur.UserName,
                                  ProfileModelID = pf.ProfileModelID,
                                  UserIDOfRecipient = pf.iUserID
                              }
                              ;
            var p = profileCard.FirstOrDefault();
            if (p.PhotoModelID != null)
            {
                
                this.Thumb = ConfigurationManager.AppSettings.Get("PhotoPath") + "thumb/" + p.PhotoModelID + ".jpg";
            }
            else
            {
                this.Thumb = "/Content/images/placeholder.png";
            }
            
            this.Area = p.area;
            this.UsernameOfReceipient = p.RecipientUsername;
            this.UserIDOfRecipient = p.UserIDOfRecipient;

            this.ProfileModelID = p.ProfileModelID;
            this.Height = EnumExtensions.EnumValueToDisplayString<Enums.Height>(p.Height);
            this.Age = AgeHelper.getAgeToday(p.dtBirthDate).ToString();
            
        }
       
    }
}