﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class ChooseGiftViewModel : CommonViewModel
    {
        public IEnumerable<GiftModel> Gifts { get; set; }
        public int GiftRecipientUserID { get; set; }
        public int GiftSenderUserID { get; set; }
        public string GiftRecipientUsername { get; set; }
        public bool HasConversationPartnerDisabledTheirProfile { get; set; }
        public bool HasConversationPartnerBlockedViewer { get; set; }
        public int CurrentNumberOfCreditsUserHas { get; set; }
        public int GiftModelID { get; set; }
        public Guid ProfileModelID { get; set; }
        public MessageModel Message { get; set; }
        public bool SendingFromProfile { get; set; }
        
        public ChooseGiftViewModel(int userID, string username, Guid profileIDOfConversationPartner, DBEntities db, HttpContextBase httpContext)
        {
            base.Setup(httpContext, userID, username, db);
            ProfileModelID = profileIDOfConversationPartner;
            ProfileModel p = db.ProfileModels.FirstOrDefault(x => x.ProfileModelID == profileIDOfConversationPartner);
            UserProfile recip = db.UserProfiles.Find(p.iUserID);

            int userIDOfConversationPartner = p.iUserID;
            GiftSenderUserID = userID;
            GiftRecipientUserID = userIDOfConversationPartner;
            Gifts = db.GiftModels;
            GiftModelID = 0;
            if (Gifts != null) {
                GiftModelID = Gifts.ElementAt<GiftModel>(0).GiftModelID;
            }
            UserProfile u = db.UserProfiles.Find(userID);
            GiftRecipientUsername = recip.UserName;
            CurrentNumberOfCreditsUserHas = u.NumberOfCreditsOwned;
            this.HasConversationPartnerDisabledTheirProfile = u.IsDisabled;
            if (db.BlockedUserModels.Count(y => y.BlockerUserID == userIDOfConversationPartner && y.BlockeeUserID == userID) > 0)
            {
                HasConversationPartnerBlockedViewer = true;
            }
        }

        public ChooseGiftViewModel() { }
    }
}