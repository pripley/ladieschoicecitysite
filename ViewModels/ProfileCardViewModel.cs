﻿using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class ProfileCardViewModel
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Area { get; set; }
        public DateTime LastLogin { get { return Utilities.TimeZoneSupport.GetLocalTime(LastLoginUTC); } }
        public DateTime LastLoginUTC { get; set; }

        public string LastLoginString { get { return Utilities.TimeZoneSupport.GetFriendlyTime(LastLoginUTC); } }
        public int Height { get; set; }
        public string HeightString { get { return EnumExtensions.EnumValueToDisplayString<Enums.Height>(Height); }  }
        public bool profile { get; set; }
        public Guid? PhotoModelID { get; set; }
        public DateTime BirthDate { get; set; }
        public Guid ProfileModelID { get; set; }
        public int DesiredMinAge { get; set; }
        public int DesireMaxAge { get; set; }
        public int MatchUserID { get; set; }
        //public Guid ProfileID { get; set; }
    }
}