﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class BlockedUsersViewModel : CommonViewModel
    {
        public List<object> BlockedUsers { get; set; }

        public BlockedUsersViewModel(HttpContextBase httpContext, int userID, string username, DBEntities db) {
            Setup(httpContext, userID, username, db);
            var blockUsers = from u in db.UserProfiles
                             join b in db.BlockedUserModels on u.UserId equals b.BlockeeUserID
                             join p in db.ProfileModels on u.UserId equals p.iUserID
                             where b.BlockerUserID == userID
                             select new
                             {
                                 BlockeeUserID = b.BlockeeUserID,
                                 BlockeeUsername = u.UserName,
                                 ProfileModelID = p.ProfileModelID
                             };
            BlockedUsers = blockUsers.ToList<object>();
        }
    }
}