﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class IndividualProfileViewModel
    {
        public Guid ProfileModelID { get; set; }
        public int UserIDOfProfile { get; set; }
        public string UsernameOfProfile { get; set; }
        public string area { get; set; }
        public string Job { get; set; }
        public string Smokes { get; set; }
        public string Drinks { get; set; }
        public string LookingFor { get; set; }
        public string Education { get; set; }
        public string Age { get; set; }
        public string Description { get; set; }
        public string Hobbies { get; set; }
        public string ValueInAPartner { get; set; }
        public string TVShows { get; set; }
        public string WhatILikeAboutMyCity { get; set; }
        public string ExtraQuestion1 { get; set; }
        public string Height { get; set; }
        public string BodyType { get; set; }
        public string Religion { get; set; }
        public string HaveChildren { get; set; }
        public string WantChildren { get; set; }
        public string Income { get; set; }
        public bool HasUserDisabledTheirAccount { get; set; }
        public int Gender { get; set; }

        public string MainPhoto { get; set; }
        public int NumMorePhotos { get; set; }
        public Guid? PhotoModelID { get; set; }
        public int iSmokes { get; set; }
        public int iDrinks { get; set; }
        public int iLookingFor { get; set; }
        public int iEducation { get; set; }
        public int iHeight { get; set; }
        public int iBodyType { get; set; }
        public int iReligion { get; set; }
        public int iHaveChildren { get; set; }
        public int iWantChildren { get; set; }
        public int iIncome { get; set; }
        public DateTime dtBirthDate { get; set; }
    }
}