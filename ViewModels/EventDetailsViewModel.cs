﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class EventDetailsViewModel : CommonViewModel
    {
        public EventModel Event { get; set; }

        public EventDetailsViewModel(HttpContextBase httpContext, int userID, string username, DBEntities db, int eventID) {
            base.Setup(httpContext, userID, username, db);
            Event = db.EventModels.FirstOrDefault(x => x.EventID == eventID);
        }
        public EventDetailsViewModel(DBEntities db, int eventID)
        {
            Event = db.EventModels.FirstOrDefault(x => x.EventID == eventID);
        }
    }
}