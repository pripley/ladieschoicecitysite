﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class IndividualConversationSummaryViewModel
    {
        public DateTime LastMessageSentDateUTC { get; set; }
        public DateTime LastMessageSentDate { get { return Utilities.TimeZoneSupport.GetLocalTime(LastMessageSentDateUTC); } }

        public string UsernameOfConversationPartner { get; set; }
        public Guid? PhotoModelID { get; set; }
        public int UserIDOfConversationPartner { get; set; }
        public string MessageText { get; set; }
        public bool Read { get; set; }
        public string ShortenedMessageText { get { return getShortenedMessageText(); } }
        public Guid ProfileModelID { get; set; }
        public string GiftImagePath { get; set; }

        private string getShortenedMessageText()
        {
 	        if (MessageText.Length > 200) {
                return MessageText.Remove(200) + "...";
            }
            else {
                return MessageText;
            }
        }
    }
}