﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class SentMessagesViewModel : CommonViewModel
    {
public IEnumerable<IndividualMessageViewModel> Messages { get; set; }
        public string ThumbPhotoPath { get; set; }
        public string UsernameOfConversationPartner { get; set; }
        public int UserIDOfConversationPartner { get; set; }
        public bool HasConversationPartnerDisabledTheirProfile { get; set; }
        public bool HasConversationPartnerBlockedViewer { get; set; }

        public SentMessagesViewModel(HttpContextBase httpContext, int userID, string username, DBEntities db)        
        {
            base.Setup(httpContext, userID, username, db);
            var conversation = from ur in db.UserProfiles
                           join m in db.MessageModels on ur.UserId equals m.RecipientUserID
                           join p in db.ProfileModels on ur.UserId equals p.iUserID into pjoined
                               from pj in pjoined.DefaultIfEmpty()
                           join ph in db.PhotoModels on ur.UserId equals ph.UserID into joined
                           join g in db.GiftModels on m.GiftModelID equals g.GiftModelID into gjoined
                               from k in gjoined.DefaultIfEmpty()
                           from j in joined.DefaultIfEmpty()
                               where (j.IsMainPhoto == true || j.IsMainPhoto == null) && 
                           m.SenderUserID == userID && m.SenderHasDeleted == false
                            orderby m.SentDate ascending
                               select new IndividualMessageViewModel()
                           {
                               SentDateUTC = m.SentDate,
                               PhotoModelID = j.PhotoModelID,
                               UsernameOfMessageSender = ur.UserName,
                               UserIDOfMessageSender = ur.UserId,
                               MessageText = m.MessageText,
                               Read = m.Read,
                               ProfileModelID = pj.ProfileModelID,
                               MessageModelID = m.MessageModelID,
                               IsWink = m.IsWink,
                               GiftImagePath = k.GiftImagePath
                           };

            this.Messages = conversation.ToList();
            this.ThumbPhotoPath = ConfigurationManager.AppSettings.Get("PhotoPath") + "thumb/";
            
        }
    }
}