﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class MyMatchesViewModel : CommonViewModel
    {
        public string OlderThan {get; set;}
        public string YoungerThan {get; set;}
        public string TallerThan {get; set;}
        public string ShorterThan {get; set;}
        public string BodyType {get; set;}
        public string Drinks {get; set;}
        public string Smokes {get; set;}
        public string MinimumIncome {get; set;}
        public string Religion {get; set;}
        public string Education {get; set;}
        public string LookingFor {get; set;}
        public string UploadPath { get; set; }
        public Guid PhotoModelID { get; set; }
        public Dictionary<string,object>[] ProfilePairs { get; set; }
        public bool IsMutualMatch { get; set; }

        public MyMatchesViewModel(HttpContextBase httpContext, int userID, string username, DBEntities db, bool mutualMatch = false)
        {
            base.Setup(httpContext, userID, username, db);
            IsMutualMatch = mutualMatch;

            MyPreferencesModel p = db.MyPreferencesModels.FirstOrDefault(x => x.UserID == userID);
            if (p == null) { p = new MyPreferencesModel(); }
            Religion = EnumExtensions.EnumMultiValuesToDisplayString<Enums.Religion>(p.Religion, true);
            if (p.TallerThan == Enums.Height.Fo0.GetHashCode() && p.ShorterThan == Enums.Height.Sv5.GetHashCode())
            {
                TallerThan = "";
                ShorterThan = "";
            }
            else
            {
                ShorterThan = EnumExtensions.EnumValueToDisplayString<Enums.Height>(p.ShorterThan);
                TallerThan = EnumExtensions.EnumValueToDisplayString<Enums.Height>(p.TallerThan);
            }
            if (p.MinimumIncome > 0)
            {
                MinimumIncome = EnumExtensions.EnumValueToDisplayString<Enums.MinimumIncome>(p.MinimumIncome);
            }
            OlderThan = p.OlderThan.ToString();
            YoungerThan = p.YoungerThan.ToString();
            Drinks = EnumExtensions.EnumMultiValuesToDisplayString<Enums.Drinks>(p.Drinks, true);
            Smokes = EnumExtensions.EnumMultiValuesToDisplayString<Enums.Smokes>(p.Smokes, true);
            Education = EnumExtensions.EnumMultiValuesToDisplayString<Enums.EducationLevel>(p.Education, true);
            LookingFor = EnumExtensions.EnumMultiValuesToDisplayString<Enums.LookingFor>(p.LookingFor, true);
            BodyType = EnumExtensions.EnumMultiValuesToDisplayString<Enums.BodyTypeMen>(p.BodyType, true);

            //int genderCode = Enums.Gender.Male.GetHashCode();
            IEnumerable<ProfileCardViewModel> profiles;
            if (IsMutualMatch)
            {
                profiles = getMyMutualMatchProfiles(db, p, userID);
            }
            else
            {
                profiles = getMyMatchProfiles(db, p, userID);
            }
            
            if (profiles.Count() == 0) { return; }
            int i = (profiles.Count() / 2) + 1;
            ProfilePairs = new Dictionary<string, object>[i];
            int rowCount = 0;
            var profileCardRow = new object[2];
            Dictionary<string, object> d;

            //ProfileModel cup = db.ProfileModels.FirstOrDefault(y => y.iUserID == userID);

            foreach (ProfileCardViewModel pm in profiles.ToList())
            {
                //pm.Age = AgeHelper.getAgeToday(pm.BirthDate);
                //if (pm.Age >= p.OlderThan && pm.Age <= p.YoungerThan && ((!mutualMatch) || (true)))
                //{
                    //pm.HeightString = EnumExtensions.EnumValueToDisplayString<Enums.Height>(pm.Height);

                    if (rowCount % 2 == 1)
                    {
                        profileCardRow[1] = pm;  //add the individual profile card to the array of profiles for the row
                        d = new Dictionary<string, object>();  //create a new hashtable to store the row of profiles
                        d.Add("Profiles", profileCardRow);
                        ProfilePairs[rowCount / 2] = d;  //add the row rof 2 profiles to the profile pairs list
                        profileCardRow = new object[2];  //create a new array to store the next row of profiles
                    }
                    else
                    {
                        profileCardRow[0] = pm;
                    }
                    rowCount++;
                //}
            }
            if (rowCount % 2 == 1)
            { // add the last profile who doesn't have a pair
                d = new Dictionary<string, object>();
                d.Add("Profiles", profileCardRow);
                ProfilePairs[rowCount / 2] = d;
            }

            UploadPath = ConfigurationManager.AppSettings.Get("PhotoPath") + "thumb/";
        
        }




        private IEnumerable<ProfileCardViewModel> getMyMatchProfiles(DBEntities db, MyPreferencesModel p, int viewerUserID)
        {
            int maleGenderCode = Enums.Gender.Male.GetHashCode();
            var profiles = from pf in db.ProfileModels
                           join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                           join ar in db.AreaOfTownModels on pf.areaID equals ar.AreaOfTownModelID into joined
                           from j in joined.DefaultIfEmpty()
                           join ph in db.PhotoModels on pf.iUserID equals ph.UserID into pjoined
                           from pj in pjoined.DefaultIfEmpty()
                           
                           //join b in db.BlockedUserModels on ur.UserId equals b.BlockeeUserID into joined3
                           //from bz in joined3.DefaultIfEmpty()
                           where (pj.IsMainPhoto == true || (pj.IsMainPhoto == null && p.ShowOnlyProfileWithPhotos == false)) && pf.Gender == maleGenderCode && (pf.Drinks & p.Drinks) == pf.Drinks
                           && (pf.Smokes & p.Smokes) == pf.Smokes && (pf.Religion & p.Religion) == pf.Religion
                            && (pf.LookingFor & p.LookingFor) > 0 && (pf.BodyType & p.BodyType) == pf.BodyType
                            && p.TallerThan <= pf.Height && p.ShorterThan >= pf.Height
                            && (pf.Education & p.Education) == pf.Education && pf.Income >= p.MinimumIncome
                            && ur.IsDisabled == false && pf.BizzaroVisibility == false
                            && ur.IsSuspectedSpammer == false
                           orderby pj.IsMainPhoto descending, ur.LastLoginDate descending


                           select new ProfileCardViewModel()
                           {
                               ProfileModelID = pf.ProfileModelID,
                               PhotoModelID = pj.PhotoModelID,
                               Area = j.Name,
                               Height = pf.Height,
                               Name = ur.UserName,
                               profile = true,
                               Age = 0,
                               BirthDate = pf.BirthDate,
                               LastLoginUTC = ur.LastLoginDate,
                               MatchUserID = ur.UserId
                           };
            List<ProfileCardViewModel> matchResults = new List<ProfileCardViewModel>();
            List<int> blockerblockeeList = getBlockedAndBlockeeList(viewerUserID, db);
            foreach (ProfileCardViewModel pm in profiles.ToList())
            {

                pm.Age = AgeHelper.getAgeToday(pm.BirthDate);
                if (pm.Age >= p.OlderThan && pm.Age <= p.YoungerThan)
                {
                    if (!blockerblockeeList.Contains(pm.MatchUserID))
                    {
                        matchResults.Add(pm);
                    }
                }
            }
            return matchResults;

        }

        private IEnumerable<ProfileCardViewModel> getMyMutualMatchProfiles(DBEntities db, MyPreferencesModel p, int userID) {
            ProfileModel cup = db.ProfileModels.FirstOrDefault(y => y.iUserID == userID);
            int viewersAge = AgeHelper.getAgeToday(cup.BirthDate);
            IQueryable<ProfileCardViewModel> profiles;
            if (IsFemale)
            {
                int genderCode = Enums.Gender.Male.GetHashCode();

                profiles = from pf in db.ProfileModels
                               join pref in db.MyPreferencesModels on pf.iUserID equals pref.UserID
                               
                               join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                               join ph in db.PhotoModels on pf.iUserID equals ph.UserID into pjoined
                               from pj in pjoined.DefaultIfEmpty()
                               join ar in db.AreaOfTownModels on pf.areaID equals ar.AreaOfTownModelID into joined
                               from j in joined.DefaultIfEmpty()

                               
                               //join b in db.BlockedUserModels on ur.UserId equals b.BlockeeUserID into joined3
                               //from bz in joined3.DefaultIfEmpty()
                               where (pj.IsMainPhoto == true || (pj.IsMainPhoto == null && p.ShowOnlyProfileWithPhotos == false)) && pf.Gender == genderCode && (pf.Drinks & p.Drinks) == pf.Drinks
                               && (pf.Smokes & p.Smokes) == pf.Smokes && (pf.Religion & p.Religion) == pf.Religion
                                && (pf.LookingFor & p.LookingFor) > 0 && (pf.BodyType & p.BodyType) == pf.BodyType
                                && p.TallerThan <= pf.Height && p.ShorterThan >= pf.Height
                                && (pf.Education & p.Education) == pf.Education && ur.IsDisabled == false
                                && (pref.Drinks & cup.Drinks) == cup.Drinks && (pref.Smokes & cup.Smokes) == cup.Smokes
                                && (pref.Religion & cup.Religion) == cup.Religion && (pref.BodyType & cup.BodyType) == cup.BodyType
                                && (pref.Education & cup.Education) == cup.Education && pref.TallerThan <= cup.Height
                                && pref.ShorterThan >= cup.Height && pf.BizzaroVisibility == false
                                && pf.Income >= p.MinimumIncome && pref.MinimumIncome <= cup.Income
                                && ur.IsSuspectedSpammer == false
                                orderby pj.IsMainPhoto descending, ur.LastLoginDate descending


                               select new ProfileCardViewModel()
                               {
                                   ProfileModelID = pf.ProfileModelID,
                                   PhotoModelID = pj.PhotoModelID,
                                   Area = j.Name,
                                   Height = pf.Height,
                                   Name = ur.UserName,
                                   profile = true,
                                   Age = 0,
                                   BirthDate = pf.BirthDate,
                                   LastLoginUTC = ur.LastLoginDate,
                                   DesiredMinAge = pref.OlderThan,
                                   DesireMaxAge = pref.YoungerThan,
                                   MatchUserID = ur.UserId
                               };
            }
            else {
                int genderCode = Enums.Gender.Female.GetHashCode();
                profiles = from pf in db.ProfileModels
                               join pref in db.MyPreferencesModels on pf.iUserID equals pref.UserID
                               join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                               join ph in db.PhotoModels on pf.iUserID equals ph.UserID into pjoined
                               from pj in pjoined.DefaultIfEmpty()
                               join ar in db.AreaOfTownModels on pf.areaID equals ar.AreaOfTownModelID into joined
                               from j in joined.DefaultIfEmpty()
                               //join b in db.BlockedUserModels on ur.UserId equals b.BlockeeUserID into joined3
                               //from bz in joined3.DefaultIfEmpty()
                               //join bb in db.BlockedUserModels on ur.UserId equals bb.BlockerUserID into joined4
                               //from bbz in joined4.DefaultIfEmpty()
                           where (pj.IsMainPhoto == true || (pj.IsMainPhoto == null && p.ShowOnlyProfileWithPhotos == false)) && pf.Gender == genderCode && (pf.Drinks & p.Drinks) == pf.Drinks
                               && (pf.Smokes & p.Smokes) == pf.Smokes && (pf.Religion & p.Religion) == pf.Religion
                                && (pf.LookingFor & p.LookingFor) > 0 && (pf.BodyType & p.BodyType) == pf.BodyType
                                && p.TallerThan <= pf.Height && p.ShorterThan >= pf.Height
                                && (pf.Education & p.Education) == pf.Education && ur.IsDisabled == false 
                                //&& bz.BlockerUserID != userID && bbz.BlockeeUserID != userID
                                && ur.IsSuspectedSpammer == false
                                && (pref.Drinks & cup.Drinks) == cup.Drinks && (pref.Smokes & cup.Smokes) == cup.Smokes
                                && (pref.Religion & cup.Religion) == cup.Religion && (pref.BodyType & cup.BodyType) == cup.BodyType
                                && (pref.Education & cup.Education) == cup.Education && pref.TallerThan <= cup.Height
                                && pref.ShorterThan >= cup.Height && pf.VisibleToMatches == true
                                && pf.Income >= p.MinimumIncome && pref.MinimumIncome <= cup.Income
                                orderby pj.IsMainPhoto descending, ur.LastLoginDate descending


                               select new ProfileCardViewModel()
                               {
                                   ProfileModelID = pf.ProfileModelID,
                                   PhotoModelID = pj.PhotoModelID,
                                   Area = j.Name,
                                   Height = pf.Height,
                                   Name = ur.UserName,
                                   profile = true,
                                   Age = 0,
                                   BirthDate = pf.BirthDate,
                                   LastLoginUTC = ur.LastLoginDate,
                                   DesiredMinAge = pref.OlderThan,
                                   DesireMaxAge = pref.YoungerThan,
                                   MatchUserID = ur.UserId
                               };
            
            }
            //List<ProfileCardViewModel> matchResults = new List<ProfileCardViewModel>();
            //List<int> blockerblockeeList = getBlockedAndBlockeeList(userID, db);
            //foreach (ProfileCardViewModel pm in profiles.ToList())
            //{
            //    pm.Age = AgeHelper.getAgeToday(pm.BirthDate);
            //    if (pm.Age >= p.OlderThan && pm.Age <= p.YoungerThan && pm.DesiredMinAge <= viewersAge && pm.DesireMaxAge >= viewersAge && pm.BlockeeUserID == null)
            //    {
            //        if (!blockerblockeeList.Contains(pm.MatchUserID)) {
            //            matchResults.Add(pm);
            //        }
            //    }
            //}
            //return matchResults;
            return refineMatchData(userID, db, profiles, viewersAge, p);
        }

        private List<int> getBlockedAndBlockeeList(int userID, DBEntities db)
        {
            List<int> blockedandblockeeList = new List<int>();
            IEnumerable<BlockedUserModel> bms = db.BlockedUserModels.Where(x => x.BlockerUserID == userID || x.BlockeeUserID == userID);
            if (bms == null) { return null; }
            foreach (BlockedUserModel b in bms)
            {
                if (b.BlockeeUserID == userID) { blockedandblockeeList.Add(b.BlockerUserID);} else {blockedandblockeeList.Add(b.BlockeeUserID);}
            }
            return blockedandblockeeList;
        }

        private List<ProfileCardViewModel> refineMatchData(int userID, DBEntities db, IQueryable<ProfileCardViewModel> profiles, int viewersAge, MyPreferencesModel p)
        {
            List<ProfileCardViewModel> matchResults = new List<ProfileCardViewModel>();
            List<int> blockerblockeeList = getBlockedAndBlockeeList(userID, db);
            foreach (ProfileCardViewModel pm in profiles.ToList())
            {
                pm.Age = AgeHelper.getAgeToday(pm.BirthDate);
                if (pm.Age >= p.OlderThan && pm.Age <= p.YoungerThan && pm.DesiredMinAge <= viewersAge && pm.DesireMaxAge >= viewersAge)
                {
                    if (!blockerblockeeList.Contains(pm.MatchUserID))
                    {
                        matchResults.Add(pm);
                    }
                }
            }
            return matchResults;
        }
    }
}
