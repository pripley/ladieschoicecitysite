﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class DeleteMyAccountModel : CommonViewModel
    {
        public string Why { get; set; }
        public string SuggestionsForImprovement { get; set; }
        public bool HasCardSaved { get; set; }
        
        public DeleteMyAccountModel(HttpContextBase httpContext, int userID, string username, DBEntities db) {
            Setup(httpContext, userID, username, db);
            UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserId == userID);
            if (u.CustomerID != null)
            {
                HasCardSaved = true;
            }

        }

        public DeleteMyAccountModel() { }
    }
}