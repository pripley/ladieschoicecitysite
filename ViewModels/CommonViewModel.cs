﻿using LadiesChoice.Models;
using LadiesChoice.Properties;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class CommonViewModel
    {
        public string city { get { return ConfigurationManager.AppSettings.Get("ActiveCity"); } }
        public string province { get { return ConfigurationManager.AppSettings.Get("Province"); } }
        public string site_name { get { return "Ladies Choice " + city;  } }
        public bool IsFemale { get; set; }
        public bool user { get; set; }
        public string FlashMessage { get; set; }
        public string NumUnReadMessages { get; set; }
        public string StatusMessage { get; set; }
        public bool ShowMenu { get; set; }
        public string FacebookGroupURL { get { return ConfigurationManager.AppSettings.Get("FacebookGroupURL"); } }
        public List<EventModel> Events { get; set; }
        public bool ShowEvents { get; set; }

        [RegularExpression("^([a-zA-Z0-9_.-]+)$", ErrorMessage = "Username must be alphanumeric")]
        public string Username { get; set; }

        public CommonViewModel() {}

        public void Setup(HttpContextBase x, int userID, string username, DBEntities db, bool showMenu = true)
        {
            ShowMenu = showMenu;
            Username = username;
            user = true;
            IsFemale = false;
            int fhc = Enums.Gender.Female.GetHashCode();

            if (db.ProfileModels.Count(n => n.iUserID == userID && n.Gender == fhc) > 0)
            {
                IsFemale = true;
            }
            if (showMenu)
            {
                var totalUnReadMessages = db.MessageModels.Where(m => m.RecipientUserID == userID && m.Read == false && m.NeedsSpamCheck == false).Count();
                if (totalUnReadMessages > 0) { NumUnReadMessages = totalUnReadMessages.ToString(); }
                ProfileModel p = db.ProfileModels.FirstOrDefault(v=>v.iUserID == userID);
                int age = AgeHelper.getAgeToday(p.BirthDate);
                //DateTime today = Utilities.TimeZoneSupport.GetLocalTime(DateTime.UtcNow);
                DateTime today = DateTime.UtcNow;
                Events = db.EventModels.Where(n => n.Publish == true && ((n.HideFromMen == false && IsFemale == false) || (n.HideFromWomen == false && IsFemale == true)) && n.MaxAge >= age && n.MinAge <= age && n.EventDate.CompareTo(today) > 0).OrderBy(j=> j.EventDate).ToList();
                if (Events.Count > 0) { ShowEvents = true; }
            }
            
            //try
            //{
            //    IsFemale = Utilities.SessionHelpers.isFemale(x);
            //}
            //catch
            //{
            //    ProfileModel p = db.ProfileModels.FirstOrDefault(y => y.iUserID == userID);
            //    if (p != null)
            //    {
            //        if (p.Gender == Enums.Gender.Female.GetHashCode()) { IsFemale = true; } else { IsFemale = false; }
            //        SessionHelpers.SaveGenderInSession(x, p.Gender);
            //    }
            //}
        }
    }
}