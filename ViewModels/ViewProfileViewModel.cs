﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LadiesChoice.ViewModels
{
    public class ViewProfileViewModel : CommonViewModel
    {
        
        public Guid ProfileModelID { get; set; }
        public int UserIDOfProfile { get; set; }
        public string UsernameOfProfile { get; set; }
        public string area { get; set; }
        public string Job { get; set; }
        public string Smokes { get; set; }
        public string Drinks { get; set; }
        public string LookingFor { get; set; }
        public string Education { get; set; }
        public string Age { get; set; }
        public string Description { get; set; }
        public string Hobbies { get; set; }
        public string ValueInAPartner { get; set; }
        public string TVShows { get; set; }
        public string WhatILikeAboutMyCity { get; set; }
        public string ExtraQuestion1 { get; set; }
        public string Height { get; set; }
        public string BodyType { get; set; }
        public string Religion { get; set; }
        public string HaveChildren { get; set; }
        public string WantChildren { get; set; }
        public string Income { get; set; }
        public bool HasUserDisabledTheirAccount { get; set; }
        public int Gender { get; set; }
        
        public string MainPhoto { get; set; }
        public int NumMorePhotos { get; set; }
        public Guid? PhotoModelID { get; set; }
        public int iSmokes { get; set; }
        public int iDrinks { get; set; }
        public int iLookingFor { get; set; }
        public int iEducation { get; set; }
        public int iHeight { get; set; }
        public int iBodyType { get; set; }
        public int iReligion { get; set; }
        public int iHaveChildren { get; set; }
        public int iWantChildren { get; set; }
        public int iIncome { get; set; }
        public DateTime dtBirthDate { get; set; }
        public int CreditCostToMessage { get { return int.Parse(ConfigurationManager.AppSettings.Get("CreditCostToMessage")); } }
        public bool CanSend { get; set; }
        public bool HasSentBefore { get; set; }
        public bool HasWinkedBefore { get; set; }
        public bool HasRecievedBefore { get; set; }
        public string WinkDateString { get; set; }

        public ViewProfileViewModel(int ProfileUserID, HttpContextBase httpContext, int viewerUserID, string username, DBEntities db)
        { 
            Setup(httpContext, viewerUserID, username, db);

            var profileView = from pf in db.ProfileModels
                              join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                              join ph in db.PhotoModels on pf.iUserID equals ph.UserID into joined
                              from j in joined.DefaultIfEmpty()
                              join ar in db.AreaOfTownModels on pf.areaID equals ar.AreaOfTownModelID into joined2
                              from z in joined2.DefaultIfEmpty()
                              //join b in db.BlockedUserModels on ur.UserId equals b.BlockerUserID into joined3
                              //from bz in joined3.DefaultIfEmpty()
                              where pf.iUserID == ProfileUserID //&& bz.BlockeeUserID != pf.iUserID
                              orderby j.IsMainPhoto descending

                              select new IndividualProfileViewModel
                              {

                                  area = z.Name,
                                  Job = pf.Job,
                                  Description = pf.Description,
                                  Hobbies = pf.Hobbies,
                                  ValueInAPartner = pf.ValueInAPartner,
                                  TVShows = pf.TVShows,
                                  ExtraQuestion1 = pf.ExtraQuestion1,
                                  WhatILikeAboutMyCity = pf.WhatILikeAboutMyCity,
                                  PhotoModelID = (Guid?)j.PhotoModelID,
                                  iReligion = pf.Religion,
                                  iDrinks = pf.Drinks,
                                  iEducation = pf.Education,
                                  iBodyType = pf.BodyType,
                                  iHaveChildren = pf.HaveChildren,
                                  iWantChildren = pf.WantChildren,
                                  iHeight = pf.Height,
                                  iIncome = pf.Income,
                                  iSmokes = pf.Smokes,
                                  dtBirthDate = pf.BirthDate,
                                  iLookingFor = pf.LookingFor,
                                  Gender = pf.Gender,
                                  UsernameOfProfile = ur.UserName,
                                  ProfileModelID = pf.ProfileModelID,
                                  UserIDOfProfile = ur.UserId,
                                  HasUserDisabledTheirAccount = ur.IsDisabled
                              }
                              ;
            IndividualProfileViewModel p = profileView.FirstOrDefault();
           
            int numPhotos = 0;
            
            if (p.PhotoModelID != null)
            {
                numPhotos = profileView.Count();
                this.NumMorePhotos = numPhotos - 1;
                this.MainPhoto = ConfigurationManager.AppSettings.Get("PhotoPath") + "thumb/" + p.PhotoModelID + ".jpg";
            }
            else
            {
                this.MainPhoto = "/Content/images/placeholder.png";
            }
            this.HasUserDisabledTheirAccount = p.HasUserDisabledTheirAccount;
            this.UserIDOfProfile = p.UserIDOfProfile;
            this.ProfileModelID = p.ProfileModelID;
            this.Job = p.Job;
            this.area = p.area;
            this.Gender = p.Gender;
            this.UsernameOfProfile = p.UsernameOfProfile;
            if (p.Description != null) { this.Description = p.Description.Replace("\n", "<br>"); }
            if (p.Hobbies != null) { this.Hobbies = p.Hobbies.Replace("\n", "<br>"); }
            if (p.ValueInAPartner != null) { this.ValueInAPartner = p.ValueInAPartner.Replace("\n", "<br>"); }
            if (p.TVShows != null) { this.TVShows = p.TVShows.Replace("\n", "<br>"); }
            if (p.ExtraQuestion1 != null) { this.ExtraQuestion1 = p.ExtraQuestion1.Replace("\n", "<br>"); }
            if (p.WhatILikeAboutMyCity != null) { this.WhatILikeAboutMyCity = p.WhatILikeAboutMyCity.Replace("\n", "<br>"); }

            this.Education = EnumExtensions.EnumValueToDisplayString<Enums.EducationLevel>(p.iEducation);
            this.Drinks = EnumExtensions.EnumValueToDisplayString<Enums.Drinks>(p.iDrinks);
            this.Smokes = EnumExtensions.EnumValueToDisplayString<Enums.Smokes>(p.iSmokes);
            this.Income = EnumExtensions.EnumValueToDisplayString<Enums.Income>(p.iIncome);
            this.Height = EnumExtensions.EnumValueToDisplayString<Enums.Height>(p.iHeight);
            this.Religion = EnumExtensions.EnumValueToDisplayString<Enums.Religion>(p.iReligion);
            this.Age = AgeHelper.getAgeToday(p.dtBirthDate).ToString();
            this.LookingFor = EnumExtensions.EnumMultiValuesToDisplayString<Enums.LookingFor>(p.iLookingFor);
            this.HaveChildren = EnumExtensions.EnumValueToDisplayString<Enums.HaveChildren>(p.iHaveChildren);
            this.WantChildren = EnumExtensions.EnumValueToDisplayString<Enums.WantChildren>(p.iWantChildren);

            if (IsFemale)
            {
                this.BodyType = EnumExtensions.EnumValueToDisplayString<Enums.BodyTypeMen>(p.iBodyType); //viewing profile so opposite gender's bodytypes
                CanSend = true;
                HasSentBefore = true;
            }
            else
            {
                this.BodyType = EnumExtensions.EnumValueToDisplayString<Enums.BodyTypeWomen>(p.iBodyType); //viewing profile so opposite gender's bodytypes
                UserProfile u = db.UserProfiles.FirstOrDefault(z => z.UserId == viewerUserID);
                HasSentBefore = false;
                if (db.MessageModels.Count(k => k.SenderUserID == viewerUserID && k.RecipientUserID == ProfileUserID && k.IsWink == false && k.GiftModelID == 0) > 0)
                {
                    HasSentBefore = true;
                }
                //if (db.MessageModels.Count(k => k.SenderUserID == viewerUserID && k.RecipientUserID == ProfileUserID && k.IsWink == true) > 0)
                //{
                //    HasWinkedBefore = true;
                //}
                MessageModel wink = db.MessageModels.FirstOrDefault(k => k.SenderUserID == viewerUserID && k.RecipientUserID == ProfileUserID && k.IsWink == true);
                if (wink != null)
                {
                    HasWinkedBefore = true;
                    WinkDateString = TimeZoneSupport.GetFriendlyTimeAlt(wink.SentDate);
                }
                if (db.MessageModels.Count(k => k.SenderUserID == ProfileUserID && k.RecipientUserID == viewerUserID) > 0)
                {
                    HasRecievedBefore = true;
                }
                
                if (CreditCostToMessage <= u.NumberOfCreditsOwned || HasSentBefore)
                {
                    this.CanSend = true;
                }
            }
        }
    }
}